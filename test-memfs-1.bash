#!/bin/bash

set -x

# load module
insmod memfs.ko

# mount filesystem
mkdir -p /mnt/dir
#-t memfs - the name of the file system
# none - specifies that no physical device is used(virtual fs)
mount -t memfs none /mnt/dir

#print all folders with specific inodes
ls -laid /mnt/dir

cd /mnt/dir

# create directory
mkdir mydir
ls -la

# create subdirectory
cd mydir
mkdir mysubdir
ls -lai

# rename subdirectory
mv mysubdir myrenamedsubdir
ls -lai

# delete renamed subdirectory
rmdir myrenamedsubdir
ls -la

# create file
touch myfile
ls -lai

# rename file
mv myfile myrenamedfile
ls -lai

# delete renamed file
rm myrenamedfile

# delete directory
cd ..
rmdir mydir
ls -la

# unmount filesystem
cd ..
umount /mnt/dir

# unload module
rmmod memfs
