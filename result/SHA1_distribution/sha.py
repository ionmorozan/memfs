import hashlib
import sys
import numpy
from collections import defaultdict
from bisect import bisect_left

count = 0

def sha1(filename):
    #create the hash
    hash = hashlib.sha1(filename)
    #digest is returned as a string of length 40
    hex_string = hash.hexdigest()

#    print 'nr bits: ' + str(hash.digest_size * 8)
#    print 'hex value: ' + hex_string
#    print 'hex value: ' + hash.digest()

    #    print'-----Hex-----'
    #get the first 8 bytes (64 bits)
    #Each hex char represents 4 bits of data
    first = hex_string[:16]
    middle = hex_string[16:32]
    last = hex_string[24:]
    #show results
#    print 'first    64: ' + first
#    print 'middle   64: ' + middle
#    print 'last     64: ' + last

    #    print'\n-----Int-----'
    #convert to int
    first_int = int(first, 16)
    middle_int = int(middle, 16)
    last_int = int(last, 16)

#    print 'first:   ' + str(first_int)
#    print 'middle:  ' + str(middle_int)
#    print 'last:    ' + str(last_int)

    return [first_int, middle_int, last_int]

def analyze(REDIS_S, file_extension):
#avg of values
    R_avg =  sum(REDIS_S)/len(REDIS_S)
    #sort array
    R_sort = sorted(REDIS_S)
#    print 'sort:' + str(sorted(REDIS_S))

    #normalize
    R_norm = []
    R_norm[:] = [x - R_avg for x in R_sort]
    print 'normalize: ' + str(R_norm)
    #    print len(R_norm)

    #min & max
    R_min = min(REDIS_S)
    R_max = max(REDIS_S)

    #    print 'min:' + str(R_min)
    #    print 'max:' + str(R_max)
    print 'avg: ' + str(R_avg)

    #count how many bins we need to plot the  variation over stdev(average)
    bins = []
    nr_bins = (R_max-R_min)/10
    bins.append(R_min - R_avg)#min

    #I have neg value so applying '/' will round it
    step= -(-(R_min - R_avg)/10 * 10)
    for i in range (nr_bins):
        bins.append(step)
        step += 10

    bins.append(R_max - R_avg)#max
    print 'bins(intervals): ' + str(bins)

    #get the uniform distribution of servers over hash
    result =  numpy.histogram(R_norm, bins=bins)
    print 'how many items in bins' + str(result)
    #    print 'len ' + str(len(result[0])) + '  ' + str(len(result[1]))
    #    print sum(result[0]) #this sum should be the same as the len of R_norm array

    #write result to .csv file
    f = open(str(file_extension) + '_deviation','w+')
    for i in range(len(result[0])):
        f.write(str((float)(result[1][i] *100)/R_avg) + ',' + str(result[0][i]) + '\n')
    f.close()

    f = open(str(file_extension) + '_distribution','w+')
    for i in range(len(REDIS_S)):
        f.write(str(i) + ',' + str(REDIS_S[i]) + '\n')
    f.close()

def compute(NR_SERVERS):
    global count

    REDIS_S_f = []
    REDIS_S_m = []
    REDIS_S_l = []

    distribution_f = [] #get the distribution with first 64bits
    distribution_m = [] #get the distribution with middle 64bits
    distribution_l = [] #get the distribution with last 64bits

    for filename in open('files'):
        filename = filename.rstrip()
        for chunk_no in range(16):
            count= count + 1
#            print filename + '_chunk' + str(chunk_no)
            hash = sha1(filename+'_chunk' + str(chunk_no))

            distribution_f.append(hash[0] % NR_SERVERS) #first 64 bits
            distribution_m.append(hash[1] % NR_SERVERS) #middle 64 bits
            distribution_l.append(hash[2] % NR_SERVERS) #last 64 bits

    print 'number of files:' + str(count)
        #    print distribution_f
    for i in range(NR_SERVERS):
        REDIS_S_f.append(distribution_f.count(i))
        REDIS_S_m.append(distribution_m.count(i))
        REDIS_S_l.append(distribution_l.count(i))

#    print '\nCompute hash distribution for 10K file names over ' + str(NR_SERVERS) + ' Redis servers'
#    print 'first 64 bits: ' + str(REDIS_S_f)
#    print 'stdev: ' + str(numpy.std(REDIS_S_f))
#    print '----Compute Stdev and distribution of Redis servers over the hash-----'
    analyze(REDIS_S_f, str(NR_SERVERS) + '_first')

#    print'\n\n'
#    print 'middle 64 bits: ' + str(REDIS_S_m)
#    print 'stdev: ' + str(numpy.std(REDIS_S_m))
#    print '----Compute Stdev and distribution of Redis servers over the hash-----'
    analyze(REDIS_S_m, str(NR_SERVERS) + '_middle')

#    print'\n\n'
#    print 'last 64 bits: ' + str(REDIS_S_l)
#    print 'stdev: ' + str(numpy.std(REDIS_S_l))
#    print '----Compute Stdev and distribution of Redis servers over the hash-----'
    analyze(REDIS_S_l, str(NR_SERVERS) + '_last')

if __name__ == "__main__":

    #NR_SERVERS=8,16,32,64,128
#    compute(8)
#    compute(16)
#    compute(32)
#    compute(64)
#    compute(128)
    compute(256)