#!/bin/bash

#Author: Ion Morozan
#
#This script computes the aggregate speed of writing files to RAM by
#using MemFS,writing to disk by using the native file system and by
#using a FUSE implementation.
#
#usage: ./script NR_TESTS  NR_OF_BLOCKS

# load module
insmod memfs.ko

# mount filesystem
mkdir -p /mnt/dir
#-t memfs - the name of the file system
# none - specifies that no physical device is used(virtual fs)
mount -t memfs none /mnt/dir


NR_TESTS=$1	# number of tests to perform
BLOCKS=$2	# number of blocks to be written (MB)
BS=1M		# block size (default: 1MB)

#generate file to write
rm -fr file
python random.py
FILE_WRITE='file'

# "---------------------"
# "      ~ Local ~      "
# "---------------------"
echo -e "\n\n LOCAL"
echo -e "----------------------WRITE----------------------------"

for (( i = 0; i < $NR_TESTS ; i++ ))
do
	OUT=$(dd if=$FILE_WRITE of=/dev/shm/file bs=$BS count=$BLOCKS 2>&1)
	echo $OUT
	OUT=`echo $OUT | sed -e  's/.*[^0-9]\([0-9]\+\)[^0-9]*$/\1/'`
	SUM=`echo $((SUM + OUT))`
done

echo -e "\nNr of tests: " $NR_TESTS
echo "After writing " $BLOCKS " blocks  of size " $BS "the aggregate speed is " $((SUM/NR_TESTS))" MB"


echo -e "------------------------READ---------------------------"
#default: /dev/null
dd of=/dev/null  if=/dev/shm/file bs=$BS count=$BLOCKS

#clean
rm /dev/shm/file



# "---------------------"
# "      ~ MemFS ~      "
# "---------------------"
echo -e "\n\n MemFS"
echo -e "------------------WRITE------------------------------"

SUM=0
for (( i = 0; i < $NR_TESTS ; i++ ))
do
	OUT=$(dd if=$FILE_WRITE of=/mnt/dir/file bs=$BS count=$BLOCKS 2>&1)
	echo $OUT
	OUT=`echo $OUT | sed -e  's/.*[^0-9]\([0-9]\+\)[^0-9]*$/\1/'`
	SUM=`echo $((SUM + OUT))`
done

echo -e "\nNr of tests: " $NR_TESTS
echo "After writing " $BLOCKS " blocks  of size " $BS "the aggregate speed is " $((SUM/NR_TESTS))" MB"


echo -e "------------------------READ---------------------------"
dd of=/dev/null  if=/mnt/dir/file bs=$BS count=$BLOCKS

#clean
rm /mnt/dir/file


#unmount
umount /mnt/dir

#remove kernel module
rmmod memfs.ko

# "---------------------"
# "      ~ RAM FUSE ~   "
# "---------------------"
echo -e "\n\n RAM FUSE"
echo -e "------------------------WRITE---------------------------"

SUM=0
for (( i = 0; i < $NR_TESTS ; i++ ))
do
	#remove file after each test
	rm -fr /root/MemFS/fuse-ramfs/testdir/file

	OUT=$(dd if=$FILE_WRITE of=/root/MemFS/fuse-ramfs/testdir/file bs=$BS count=$BLOCKS 2>&1)
	echo $OUT
	OUT=${OUT#*s,} #get the last number
	OUT=`echo $OUT | awk -F' ' '{print $1;}'` #remove any other string after the number
	SUM="$(echo "$SUM+$OUT"|bc)"

	#remove file after each test
	#rm /root/MemFS/fuse-ramfs/testdir/file
done

AVG=$(echo "$SUM/$NR_TESTS" | bc -l | awk '{printf("%4.2f", $1);}')
echo -e "\nNr of tests: " $NR_TESTS
echo "After writing " $BLOCKS " blocks  of size " $BS "the aggregate speed is " $AVG " MB"


echo -e "------------------------READ---------------------------"
dd of=/dev/null  if=/root/MemFS/fuse-ramfs/testdir/file bs=$BS count=$BLOCKS

#clean
rm -fr /root/MemFS/fuse-ramfs/testdir/file

rm -fr file
