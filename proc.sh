#!/bin/bash
#set this script to run only on 0 cpu (first cpu)
#run this script like this: taskset -c 0 ./proc.ch

truncate -s 0 cpu_avg

refresh_rate=0.1

#set affinity on cpu 1 2 3
taskset -c 1,2,3 ./dd.sh 1 > speed_out &


load_cpu0 () {
	read cpu a b c previdle_all rest < /proc/stat
	prevtotal_all=$((a+b+c+previdle_all))

	#take the second line of file /proc/stat and assign to vars
	read cpu a b c previdle_cpu0 rest <<<  `awk 'NR==2'  /proc/stat`
	prevtotal_cpu0=$((a+b+c+previdle_cpu0))
}

get_load_cpu(){
	read cpu a b c idle_all rest < /proc/stat
	total_all=$((a+b+c+idle_all))
	CPU=$((100*( (total_all-prevtotal_all) - (idle_all-previdle_all) ) / (total_all-prevtotal_all)))
	#echo $CPU

	read cpu a b c idle_cpu0 rest <<<  `awk 'NR==2'  /proc/stat`
	total_cpu0=$((a+b+c+idle_cpu0))

	CPU_0=$((100*( (total_cpu0-prevtotal_cpu0) - (idle_cpu0-previdle_cpu0) ) / (total_cpu0-prevtotal_cpu0)))
	if [ "$dif" -gt 0 ]
	then
		CPU_0=$((CPU_0/4))
	else
		CPU_0=0

	fi
}

sleep 1
while [[ `pgrep -f "dd.sh"` ]];
#while [[ `pgrep -f "init"` ]];
do
	
	load_cpu0
	sleep $refresh_rate
	get_load_cpu
	
	#echo 'overall load: ' $CPU   'load on 0 CPU: ' $CPU_0
	
	dif=$((CPU-CPU_0))
	if [ "$dif" -gt 0 ] 
	then
		echo 'Load:' $((CPU-CPU_0)) '%'
		echo $((CPU-CPU_0)) >> cpu_avg
#	else
#		break
	fi
done

echo 'AVG:' `awk '{ total += $1 } END { print total/NR }' cpu_avg` '%' 
