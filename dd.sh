#!/bin/bash
# ./dd.sh <nr_of_blocks>
nrTests=1

nrBlocks=$1
nrFiles=8

chunkSize=1M
chunkS=1

#sar -u 2 5
for (( j = 0; j < $nrTests ; j++ )) do
	TIMEFORMAT="%R";
	OUT=$(time (
			for (( i = 0; i < $nrFiles ; i++ )) do
				#write
				#dd if=/dev/zero of=~/dir/file$i bs=$chunkSize count=$nrBlocks 2>/dev/null &
				#read
				dd if=~/dir/file$i of=/dev/null bs=$chunkSize count=$nrBlocks 2>/dev/null &
			done;
		wait) 2>&1
	)

	echo "time real:"  $OUT  " s"
	
	#how much data in MB
	F=`echo $nrFiles*$chunkS*$nrBlocks`
	
	#individual speed for one test pf $nrfiles
	INDIVIDUAL=`echo  $F/$OUT | bc `
	echo -e "individual speed:"  $INDIVIDUAL "MB/s \n"

	#compute the agregate speed
	SUM=`echo $((SUM + INDIVIDUAL))`
	#echo $SUM

	#umount + mount
	#sleep 1
	#umount ~/dir ; rmmod memfsins ; insmod memfsins.ko ; mount -t memfs none ~/dir
	#sleep 0.5
done

echo "Run: " $nrTests "times" 
echo "After writing " $nrFiles " files consisting of " $nrBlocks " blocks  of size " $chunkSize "the aggregate speed is " $((SUM/nrTests))" MB/s"
