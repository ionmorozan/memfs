#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>
#include <net/inet_common.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>


#include <linux/fcntl.h>
#include <linux/dcache.h>
#include <linux/file.h>


/************* Socket Communication **************/
static int init_socket( void )
{
	int err = 0;
	/* create socket */
	if( sock_create(PF_INET, SOCK_STREAM, IPPROTO_TCP, &clientsocket) < 0 ){
		printk( LOG_LEVEL "server: Error creating clientsocket.n" );
		return -EIO;
	}
	
	memset(&to,0, sizeof(to));
	
	/* destination address */
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = in_aton( IP );
	to.sin_port = htons( (unsigned short) SERVERPORT );

	if( (err = inet_stream_connect(clientsocket, (struct sockaddr*)&to, sizeof(struct sockaddr_in), 0)) < 0){
		printk( LOG_LEVEL "server: Error establishing connection to the server %d", err);
		return -EIO;
	}

	return 1;
}

static int exit_socket( void ){

	/* release socket  */
	if( clientsocket ){
		sock_release( clientsocket );
		clientsocket = NULL;
	}
	return 1;
}


static int  send_socket( struct socket *sock, struct sockaddr_in *to, unsigned char *buf, int len )
{

	struct msghdr msg;
	struct iovec iov;
	mm_segment_t oldfs;

	int size = 0;

	if (sock->sk == NULL)
		return 0;

	/* add data to the message */
	iov.iov_base 		= buf;
	iov.iov_len  		= len;
	
	/* create  the packet */
	memset(&msg,0,sizeof(msg));


	msg.msg_flags 		= 0;
	msg.msg_name 		= 0;
	msg.msg_namelen 	= 0;
	msg.msg_control 	= NULL;
	msg.msg_controllen 	= 0;
	msg.msg_iov    		= &iov;
	msg.msg_iovlen 		= 1;



	oldfs = get_fs();
	set_fs( KERNEL_DS );
	size = sock_sendmsg( sock, &msg, len );
	set_fs( oldfs );

	printk( LOG_LEVEL "sock_sendmsg returned: size=%d  len=%d\n", size, len);

	return 0;
}

/**************** End socket communication **************/
