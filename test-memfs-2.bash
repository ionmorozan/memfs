# !/bin/bash

set -x

# load module
insmod memfs.ko

# mount fiilesystem
# -p create parents dir (if does not exist)
mkdir -p /mnt/dir
mount -t memfs none /mnt/dir
ls -laid /mnt/dir

cd /mnt/dir

# create file
touch myfile
ls -lai

# rename file
mv myfile myrenamedfile
ls -lai

# create link to file
link myrenamedfile mylink
ls -lai

# read/write file
echo "message" > myrenamedfile
cat myrenamedfile

# remove link to file
unlink mylink
ls -la

# delete file
rm -f myrenamedfile
ls -la

# unmount filesystem
cd ..
umount /mnt/dir

# unload module
rmmod memfs
