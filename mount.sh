#!/bin/bash

#read REDIS IP configuration file
while read line; do
	redis_servers+=("$line")
done < redis.ip

#print all redis server sent as input
echo 'Redis servers: ' ${redis_servers[*]}

#unmount module
umount ~/dir ; rmmod memfsins ;


#insert module and give as input list of redis servers
insmod memfsins.ko REDIS_SERVER_IP=$(IFS=,; echo "${redis_servers[*]}"); mount -t memfs none ~/dir
