#!/bin/bash

#erase redis db on remote with authentication
#ssh root@10.100.0.7 "redis-cli -a memfs flushdb"
#ssh root@10.100.0.10 "redis-cli -a memfs flushdb"
#ssh root@10.100.0.38 "redis-cli -a memfs flushdb"
#ssh root@10.100.0.39 "redis-cli -a memfs flushdb"

ssh root@10.100.0.7 "redis-cli  flushdb"
ssh root@10.100.0.10 "redis-cli flushdb"
ssh root@10.100.0.38 "redis-cli flushdb"
ssh root@10.100.0.39 "redis-cli flushdb"
