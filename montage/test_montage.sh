#! /bin/bash

export MOUNT_DIR=$1

mkdir $MOUNT_DIR/images
cp /mnt/small_montage/*.fits $MOUNT_DIR/images/

time mImgtbl $MOUNT_DIR/images $MOUNT_DIR/images.tbl
time mMakeHdr $MOUNT_DIR/images.tbl $MOUNT_DIR/newregion.hdr

mkdir $MOUNT_DIR/projdir

for file in `ls $MOUNT_DIR/images` ;
do
	time mProjectPP $MOUNT_DIR/images/$file $MOUNT_DIR/projdir/hdu0_${file} $MOUNT_DIR/newregion.hdr >> small_montage_out.txt
	
	
done


