#!/bin/bash

#clean redis databases
#./clear_redis_db.sh

#run tests -> ./dd.sh <nr_of_blocks> of 1MB
#taskset -c 1,2,3 ./dd.sh 256 > speed_out &
./dd.sh 256 > speed_out &

#get pid of dd.sh
#ps -eo pid,command | grep "dd.sh" | grep -v grep | awk '{print $1}'
PID=$(ps -C dd.sh -o pid=)

#monitor dd functions with pidstat
echo $PID
pidstat  -C "dd" -p ALL -I 1 16 > pid_out

#pidstat -p $PID   1

#compute the average of cpu load
SYS=$(cat pid_out | grep 'Average' | tr -s ' ' | cut -d ' ' -f 5 | sed "1 d" | paste -sd+ - | bc)

#display output
echo 'Average Kernel load is : ' $SYS '%'

#clean
#rm pid_out


