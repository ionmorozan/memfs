/*
 * In-Memory File System (MemFS)
 * V 0.2
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/in.h>
#include <linux/inet.h>
#include <net/sock.h>
#include <net/inet_common.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/fcntl.h>
#include <linux/dcache.h>
#include <linux/file.h>
#include <linux/crypto.h>
#include <linux/types.h>

#include <linux/pid.h>
#include "redisclient/redisclient.h"

MODULE_DESCRIPTION("MemFS");
MODULE_AUTHOR("ION MOROZAN <ion.morozan@gmail>");
MODULE_LICENSE("GPL");

/*tainting operations*/
#define READ		1
#define WRITE		2
#define SIZE		3
#define DIROPS		4
#define	DELETE		5

/* auxiliary information*/
#define MEMFS_MAGIC	0xdeadbeef
#define LOG_LEVEL	KERN_ALERT
#define PRINT		0
#define CHUNK		1048576//524288//
#define IP_LENGTH	20
#define LAST_INO_BATCH	1024
static DEFINE_PER_CPU(unsigned int, last_ino);


/* Redis communication */
#define SERVER_PORT	6379
#define NR_CLIENTS	16
#define PASSWD		"memfs"


/*---------------------------------------*/
struct redis_servers{
	char ip[20];
	struct socket* sock[NR_CLIENTS];
	int fd[NR_CLIENTS];
	int lock[NR_CLIENTS];

	long inode_no[NR_CLIENTS];
	char f_name[NR_CLIENTS][500];
	unsigned f_size[NR_CLIENTS];

	char *buffer[NR_CLIENTS];
	int buf_pos[NR_CLIENTS];
	
	int taint[NR_CLIENTS];
	int pid[NR_CLIENTS];
};

/*information received as command line parameters*/
static char *REDIS_SERVER_IP[IP_LENGTH];
static int NR_SERVERS;
module_param_array(REDIS_SERVER_IP, charp, &NR_SERVERS, 0);

/*Struct which contains information about the redis servers*/
static struct redis_servers *redis;

/*synchronization primitives*/
static DEFINE_MUTEX(redis_lock);
static DEFINE_MUTEX(client);
static DEFINE_MUTEX(processpid);
static struct semaphore *mr_sem;
/*---------------------------------------*/

static const struct super_operations memFS_ops = {
	.statfs		= simple_statfs,
	.drop_inode	= generic_delete_inode,
};

static struct inode_operations memFS_dir_inode_operations;
static const struct file_operations memFS_file_operations;
static struct inode_operations memFS_file_inode_operations;
static const struct address_space_operations memFS_aops;
static const struct file_operations memFS_dir_operations;

static void get_full_path_of_file(struct dentry *dentry, char full_path[]);
struct inode *memFS_get_inode(struct super_block *sb, int mode);
int simple_setsize(struct inode *inode, loff_t newsize);
static void assert(int isError, char* message){
	if (isError){
		////////printk(LOG_LEVEL "Redis error: %s\n", message);
	}

	return;
}

static void debug(int print, char* message){
	if (print){
		////////printk(LOG_LEVEL "%s\n", message);
	}

	return;
}
/**************** Redis communication **************/
static int redis_exit( int r_id, int c_id ){

	/* release socket  */
	if(redis[r_id].sock[c_id] != NULL){
		inet_release( redis[r_id].sock[c_id] );
		redis[r_id].sock[c_id] = NULL;
	}
	return 1;
}

static void redis_init(int r_id, int c_id)
{
	redisReply *reply;
	int fd;
	
	redis[r_id].sock[c_id] = NULL;
	reply = redisConnect(&redis[r_id].sock[c_id], REDIS_SERVER_IP[r_id], SERVER_PORT);

	if (reply != NULL) {
		return;
	}

	/*Authneticate client with pre-shared password*/
	//fd = sock_map_fd(redis[r_id].sock[c_id], 0);
	//reply = redisCommand(fd, "AUTH %s", PASSWD);
	////////printk(LOG_LEVEL "AUTH REPLY: %s\n", reply->reply);

	return;
}

static int get_free_channel(int r_id){
	int i;

	////////printk(LOG_LEVEL"**************GET FREE CHANNEL***************\n");
	down(&mr_sem[r_id]);
	for ( i = 0; i < NR_CLIENTS; i++){
		mutex_lock(&redis_lock);
		if(redis[r_id].lock[i] == 0){
			redis[r_id].lock[i] = 1;
			mutex_unlock(&redis_lock);
			return i;
		}
		mutex_unlock(&redis_lock);
	}

	////////printk(LOG_LEVEL "NO FREEE CHANNEEEL\n");
	return -1;
}

/**
 * mark the channel as being free
 */
static void release_channel(int r_id, int c_id){
	
	////////printk(LOG_LEVEL "**************RELEASE CHANNEL***************\n");
	
	redis[r_id].lock[c_id] = 0;
	redis[r_id].fd[c_id] = -1;
	redis[r_id].f_size[c_id] = 0;
	redis[r_id].inode_no[c_id] = -1;
	redis[r_id].taint[c_id] = -1;
	redis[r_id].pid[c_id] = -1;
	memset(redis[r_id].f_name[c_id], 0, 500 * sizeof(char));// = '\0';

	memset(redis[r_id].buffer[c_id], 0, CHUNK * sizeof(char));
	redis[r_id].buf_pos[c_id] = 0;
	
	/* notify a sleeping thread(if any) that a new slot is avaialbe */
	up(&mr_sem[r_id]);
}

/*
 * Identifies the location of a file on a Redis server
 * applying the SHA1 hashing algorithm.
 */
static int
identify_server(const char *plaintext)
{
	struct scatterlist sg;
	struct hash_desc desc;

	char hash[41];
	unsigned int  len = strlen(plaintext);
	
	sg_init_one(&sg, plaintext, len);
	desc.tfm = crypto_alloc_hash("sha1", 0, CRYPTO_ALG_ASYNC);

	crypto_hash_init(&desc);
	crypto_hash_update(&desc, &sg, len);
	crypto_hash_final(&desc, hash);

	crypto_free_hash(desc.tfm);


	uint64_t  value = 0;
	/* take the last 64 bits of the hash*/
	memcpy(&value, hash + 12, 8);
	
	////////printk(LOG_LEVEL "-------------identify server ------------\n");
	////////printk(LOG_LEVEL "file: %s hash: %s \n", plaintext, hash);
	////////printk(LOG_LEVEL "value: %llu\n", value);
	////////printk(LOG_LEVEL "server: %llu   server ip: %s\n", value % NR_SERVERS, REDIS_SERVER_IP[value % NR_SERVERS]);
	////////printk(LOG_LEVEL "-------------end hash------------\n");
	
	return value % NR_SERVERS;
}


/**
 * name : file name that the client is using
 * r_id : number of redis server
 * taint: can be a value 1..5 representing the operation on the file
 * @return: the position in the redis array
 */
static int identify_client(long inode_no, int r_id, int taint){
//static int identify_client(long inode_no, int r_id, int taint, int pid){

	int i = 0;

	////////printk(LOG_LEVEL "**************IDENTIFY CLIENT***************\n");

	mutex_lock(&client);
	int pid = task_pid_nr(current);
	for (i = 0; i < NR_CLIENTS; i++){
		////////printk(LOG_LEVEL "inode: %d   redis inode: %d\n",inode_no, redis[r_id].inode_no[i]);
		if((inode_no == redis[r_id].inode_no[i]) && (taint == redis[r_id].taint[i]) && (pid == redis[r_id].pid[i])){
			////////printk(LOG_LEVEL "FILE: %s   CLIENT: %d\n",name, i);
			mutex_unlock(&client);
			//return redis.fd[i];
			return i;
		}
	}
	mutex_unlock(&client);
	////////printk(LOG_LEVEL "NO CLIENT FOUND for inode %d   taint : %d  :-1\n", inode_no, taint);
	return -1;
}


static void remove_file_from_redis(long  inode_no, unsigned size, int r_id){
	redisReply *reply;
	int chunkNo = 0;
	char key[500];
	int fd = 0;
	int c_id = 0;

	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DELETE, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DELETE)) == -1){
		////////printk(LOG_LEVEL"ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	////////printk(LOG_LEVEL "**************REDIS: REMOVE***************\n");
	////////printk(LOG_LEVEL "file size: %d\n",size);
	////////printk(LOG_LEVEL "file name: %s\n",name);


	/* delete each chunk*/
	for(chunkNo = 1; chunkNo <= size / CHUNK + 1; chunkNo++){
		sprintf(key, "%s_chunk%d", redis[r_id].f_name[c_id], chunkNo);
		////////printk(LOG_LEVEL "what I delete:  %s\n",key);
	
		/*identify each individual chunk on its assigned redis
		 * server*/
		int ch_r_id = identify_server(key);
		int ch_c_id = get_free_channel(ch_r_id);
		
		redis[ch_r_id].taint[ch_c_id] = DELETE;
		redis[ch_r_id].pid[ch_c_id] = task_pid_nr(current);
		redis[ch_r_id].inode_no[ch_c_id] = inode_no;
		if(redis[ch_r_id].sock[ch_c_id]){
			////////printk(LOG_LEVEL "socket not null%d\n",redis.sock[c_id]);
		}
		redis[ch_r_id].fd[ch_c_id] = sock_map_fd(redis[ch_r_id].sock[ch_c_id], 0);	
		fd = redis[ch_r_id].fd[ch_c_id];


		reply = redisCommand(fd, "DEL %s", key);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		freeReplyObject(reply);

		key[0] = '\0';
	
		//release file descriptor
		sys_close(redis[ch_r_id].fd[ch_c_id]);
	
		//release communication channel to reids
		release_channel(ch_r_id, ch_c_id);
	}	

	/* delete  size */
	sprintf(key, "%s", redis[r_id].f_name[c_id]);
	/* assign channel */
	fd = redis[r_id].fd[c_id];

	reply = redisCommand(fd, "DEL %s", key);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	freeReplyObject(reply);
	
	return;

}


static void read_from_redis(struct file *filp, long inode_no, unsigned char* file, unsigned start, unsigned length,  unsigned char* file_path){
	redisReply *reply;
	char key[500];

	int chunkNo = 0;
	int offset = 0;
	int to_read = 0;
	int progress = 0;

	//int c_id = 0;
	int fd = 0;
	
	////printk(LOG_LEVEL "***************************REDIS: READ***************************\n");
	
	chunkNo = start / CHUNK + 1;
	offset  = start % CHUNK;
	to_read = CHUNK - offset;
	if(to_read > length){
		to_read = length;
	}

	int  previous_chunkNo = filp->f_dentry->d_inode->i_version;
	if((previous_chunkNo != 0) && (previous_chunkNo != chunkNo)){
		/* identify the file stripe that we have to create */
		char previous_key[500];
		sprintf(previous_key, "%s_chunk%d", file_path, previous_chunkNo);
		
		int prev_ch_r_id = identify_server(previous_key);
	//	mutex_lock(&processpid);
	//		int pid = task_pid_nr(current);
	//		int prev_ch_c_id = identify_client(-inode_no, prev_ch_r_id, READ, pid);
			int prev_ch_c_id = identify_client(-inode_no, prev_ch_r_id, READ);
	//	mutex_unlock(&processpid);
		////////printk(LOG_LEVEL "current chunk :%d , previous chunk :%d server:%d\n", chunkNo, previous_chunkNo, prev_ch_r_id, prev_ch_r_id);
		
		if ( prev_ch_c_id != -1){
			////////printk(LOG_LEVEL "Release previous chunk: %s\n", previous_key);
			release_channel(prev_ch_r_id, prev_ch_c_id);
		}
		//}
	}

	/* identify the file stripe that we have to create */
	sprintf(key, "%s_chunk%d", file_path, chunkNo);
	////////printk(LOG_LEVEL "********REDIS: READ*********%s*******previous chunk:%d***********\n", key, previous_chunkNo);
	////////printk(LOG_LEVEL "key: %s \n", key);
	
	/*identify each individual chunk on its assigned redis server*/
	int ch_r_id = identify_server(key);
	int ch_c_id = -1;

	mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if ((ch_c_id=identify_client(-inode_no, ch_r_id, READ, pid)) == -1){
	if ((ch_c_id=identify_client(-inode_no, ch_r_id, READ)) == -1){
	//mutex_unlock(&processpid);
		ch_c_id = get_free_channel(ch_r_id);
		////////printk(LOG_LEVEL "~~~~~~~~OBTAIN FILE HANDLER~~~%s~~~~~~~~-%d~~~~\n",key, inode_no );

		/* copy the name if the file*/
		sprintf(redis[ch_r_id].f_name[ch_c_id], "%s", key);	
		/*taint for READ*/
		redis[ch_r_id].taint[ch_c_id] = READ;
		redis[ch_r_id].pid[ch_c_id] = task_pid_nr(current);
		/* get the local inode number*/
		redis[ch_r_id].inode_no[ch_c_id] = -inode_no;
		////////printk(LOG_LEVEL "inode number : %d \n", redis[ch_r_id].inode_no[ch_c_id]);
		if(redis[ch_r_id].sock[ch_c_id]){
			////////printk(LOG_LEVEL "socket not null%d\n",redis.sock[c_id]);
		}
		redis[ch_r_id].fd[ch_c_id] = sock_map_fd(redis[ch_r_id].sock[ch_c_id], 0);
	mutex_unlock(&processpid);
		////////printk(LOG_LEVEL "client position : %d obtained fd: %d \n", ch_c_id, redis[ch_r_id].fd[ch_c_id]);

		/*get(read) CHUNK from the network*/
		reply = redisCommand(redis[ch_r_id].fd[ch_c_id],
					"GET %s", 
					key);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		
		/*if 'key' does not exist, then we try to get  this data from local cache*/
		if (reply->type == REDIS_REPLY_NIL){
			//////printk(LOG_LEVEL "No key:%s \n", key);
			int w_r_id = identify_server(key);
			
			//mutex_lock(&processpid);
			//	int pid = task_pid_nr(current);
			//	int w_c_id = identify_client(-inode_no, w_r_id, WRITE, pid);
				int w_c_id = identify_client(-inode_no, w_r_id, WRITE);
			//mutex_unlock(&processpid);
			////printk(LOG_LEVEL "Fetch data from local cache client!@//: %d \n", w_c_id);

			memcpy(redis[ch_r_id].buffer[ch_c_id], redis[w_r_id].buffer[w_c_id],redis[w_r_id].buf_pos[w_c_id]);
			//////printk(LOG_LEVEL "Info about local cache  :%s    :%d\n",redis[w_r_id].f_name[w_c_id], redis[w_r_id].inode_no[w_c_id]);
			//////printk(LOG_LEVEL "LENTGH: %d how much do i have cached :%d\n", length, redis[w_r_id].buf_pos[w_c_id]);
			////////printk(LOG_LEVEL "What we read from local: %s\n",redis[w_r_id].buffer[w_c_id]);
			
			redis[ch_r_id].buf_pos[ch_c_id] = redis[w_r_id].buf_pos[w_c_id];
		}else{
		
			int copy_len = CHUNK;
                        if (filp->f_dentry->d_inode->i_size < (chunkNo*CHUNK))
                                copy_len -= ((chunkNo*CHUNK) - filp->f_dentry->d_inode->i_size);

			////printk(LOG_LEVEL "Normal copy  copy_len:%d\n",copy_len);
			/* copy the new data to the cache buffer */
			//memcpy(redis[ch_r_id].buffer[ch_c_id], reply->reply, strlen(reply->reply));
			memcpy(redis[ch_r_id].buffer[ch_c_id], reply->reply, copy_len);
			redis[ch_r_id].buf_pos[ch_c_id] = copy_len;
			//redis[ch_r_id].buf_pos[ch_c_id] = strlen(reply->reply);
		}

		////////printk(LOG_LEVEL "buf len:  : %d \n", redis[ch_r_id].buf_pos[ch_c_id]);
		////////printk(LOG_LEVEL " what did we read: %s\n", redis[ch_r_id].buffer[ch_c_id]);
		freeReplyObject(reply);

		sys_close(redis[ch_r_id].fd[ch_c_id]);
	}else{	
		mutex_unlock(&processpid);
	}

	////////printk(LOG_LEVEL "offset: %d\n", offset);
	////////printk(LOG_LEVEL "start: %d\n", start);
	////////printk(LOG_LEVEL "to_read: %d\n", to_read);
	////////printk(LOG_LEVEL "length: %d\n", length);
	////////printk(LOG_LEVEL "progress: %d\n", progress);

/*
	if (redis[ch_r_id].buf_pos[ch_c_id] > 0 && strcmp(redis[ch_r_id].f_name[ch_c_id], key)!=0){
			redis[ch_r_id].buf_pos[ch_c_id] = 0;
			////////printk(LOG_LEVEL "Random access of chunk \n");
			//release file descriptor
			//sys_close(redis[ch_r_id].fd[ch_c_id]);
			//release communication channel to reids
			//release_channel(ch_r_id, ch_c_id);
	

	}
*/
	////////printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	while(length > 0){

		////printk(LOG_LEVEL "current key :%s     buf pos: %d length: %d to_read:%d\n", key, redis[ch_r_id].buf_pos[ch_c_id], length, to_read);
		if (redis[ch_r_id].buf_pos[ch_c_id] == 0){
			
			chunkNo++;
			sprintf(key, "%s_chunk%d", file_path, chunkNo);

			//////printk(LOG_LEVEL "~~~~~~~~GO TO NEXT CHUNK~~:%s~~\n", key);
			/*since we finished the data from the buffer release connection to redis*/
			//release file descriptor
			//sys_close(redis[ch_r_id].fd[ch_c_id]);
			//release communication channel to reids
			release_channel(ch_r_id, ch_c_id);
	
			////////printk(LOG_LEVEL "release communication channel to reids key: %s   client:%d\n", key, ch_c_id);

			/*identify each individual chunk on its assigned redis*/
			ch_r_id = identify_server(key);
		 mutex_lock(&processpid);
			ch_c_id = get_free_channel(ch_r_id);
	
			/* copy the name if the file*/
			sprintf(redis[ch_r_id].f_name[ch_c_id], "%s", key);	
			/*taint for READ */
			redis[ch_r_id].taint[ch_c_id] = READ;
			redis[ch_r_id].pid[ch_c_id] = task_pid_nr(current);
			/* get the local inode number*/
			redis[ch_r_id].inode_no[ch_c_id] = -inode_no;

			if(redis[ch_r_id].sock[ch_c_id]){
				////////printk(LOG_LEVEL "socket not null%d\n",redis.sock[c_id]);
			}
			redis[ch_r_id].fd[ch_c_id] = sock_map_fd(redis[ch_r_id].sock[ch_c_id], 0);
			
		 mutex_unlock(&processpid);
			/*get(read) CHUNK from the network*/
			reply = redisCommand(redis[ch_r_id].fd[ch_c_id],
						"GET %s", 
						key);
			assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
			sys_close(redis[ch_r_id].fd[ch_c_id]);

			/*if 'key' does not exist, then we try to get  this data from local cache*/
			if (reply->type == REDIS_REPLY_NIL){
				int w_r_id = identify_server(key);
				//mutex_lock(&processpid);
				//	int pid = task_pid_nr(current);
				//	int w_c_id = identify_client(-inode_no, w_r_id, WRITE, pid);
					int w_c_id = identify_client(-inode_no, w_r_id, WRITE);
				//mutex_unlock(&processpid);

				////printk(LOG_LEVEL "Fetch data from local cache CLIENT:%s    pos:%d \n", key, w_c_id);
				if(w_c_id != -1){
					memcpy(redis[ch_r_id].buffer[ch_c_id], redis[w_r_id].buffer[w_c_id],redis[w_r_id].buf_pos[w_c_id]);
					redis[ch_r_id].buf_pos[ch_c_id] = redis[w_r_id].buf_pos[w_c_id];
				}else{
					freeReplyObject(reply);
					release_channel(ch_r_id, ch_c_id);
					break;
				}
			}else{
				int copy_len = CHUNK;
	                        if (filp->f_dentry->d_inode->i_size < (chunkNo*CHUNK))
        	                        copy_len -= ((chunkNo*CHUNK) - filp->f_dentry->d_inode->i_size);

				////printk(LOG_LEVEL "Available size: filename: %s copy_len :%d file size:%d" ,key, copy_len, filp->f_dentry->d_inode->i_size);

				if(copy_len == 0){
					freeReplyObject(reply);
					release_channel(ch_r_id, ch_c_id);
					break;
				}
				/* copy the new data  to the buffer*/
				//memcpy(redis[ch_r_id].buffer[ch_c_id], reply->reply, strlen(reply->reply));
				memcpy(redis[ch_r_id].buffer[ch_c_id], reply->reply, copy_len);
				//redis[ch_r_id].buf_pos[ch_c_id] = strlen(reply->reply);
				redis[ch_r_id].buf_pos[ch_c_id] = copy_len;//strlen(reply->reply);
			}

			
			freeReplyObject(reply);

			////////printk(LOG_LEVEL "NEW client %d   server %d \n",ch_c_id, ch_r_id);
			////////printk(LOG_LEVEL " size of what we read: %d\n", redis[ch_r_id].buf_pos[ch_c_id]);
			////////printk(LOG_LEVEL " what did we read: %s\n", redis[ch_r_id].buffer[ch_c_id]);
			// go to the next chunk from redis  //???? do
			//I have to go to the next chunk???
			//chunkNo++;
			//sprintf(key, "%s_chunk%d", file_path, chunkNo);
			////////printk(LOG_LEVEL " next chunk: %s\n", key);
		}
		/*check to see if by reading we overflow the cache  buffer*/
		if ( (redis[ch_r_id].buf_pos[ch_c_id] - to_read) < 0 ){
			////////printk(LOG_LEVEL "difference :%d ", redis[ch_r_id].buf_pos[ch_c_id] - to_read);
			to_read = redis[ch_r_id].buf_pos[ch_c_id];
			////////printk(LOG_LEVEL "overflow: to_read shrinked: %d", to_read);
		}
	

		////////printk(LOG_LEVEL " what did we read: %s\n", redis[ch_r_id].buffer[ch_c_id]);
		////////printk(LOG_LEVEL "client %d   server %d \n",ch_c_id, ch_r_id);
		////////printk(LOG_LEVEL "to_read  %d    \n",to_read);
		/* copy specific part from chunk*/
		//memcpy(file + progress, reply->reply + offset, to_read);
		//memcpy( file ,//+ progress,
		//	redis[ch_r_id].buffer[ch_c_id], //+ offset, 
		//	to_read
		//);
		unsigned long success = 0;
		success = copy_to_user( file + progress,
			redis[ch_r_id].buffer[ch_c_id] + offset, 
			to_read
		);
		////////printk(LOG_LEVEL "READ success  %d    \n",success);
		//???? do not use strlen -> when /dev/zero is issued
		//then strelen will be always 0
		//if (strlen(redis[ch_r_id].buffer[ch_c_id]) == 0){
/*		if (!memcmp (testblock, redis[ch_r_id].buffer[ch_c_id], CHUNK) == 0){
			////////printk(LOG_LEVEL "Fetch data from local cache  \n");
			int w_r_id = identify_server(key);
			int w_c_id = identify_client(-inode_no, w_r_id, WRITE);
			////////printk(LOG_LEVEL "Info about local cache  :%s    :%d\n",redis[w_r_id].f_name[w_c_id], redis[w_r_id].inode_no[w_c_id]);
			////////printk(LOG_LEVEL "What we read from local: %s\n",redis[w_r_id].buffer[w_c_id]);
			copy_to_user( file + progress,
				redis[w_r_id].buffer[w_c_id] + offset, 
				to_read
			);

			//release_channel(ch_r_id, ch_c_id);
			//break;

		}
*/
		redis[ch_r_id].buf_pos[ch_c_id] -= to_read;


		progress +=to_read;
		length   -=to_read;
		offset    = 0;
		//key[0] = '\0';

		////////printk(LOG_LEVEL "~~~~~~~~~left :%d \n", length);

		if(length / CHUNK > 0){
			to_read = CHUNK;
		}else{
			to_read = length;
		}
	}
	
	filp->f_dentry->d_inode->i_version = chunkNo;

	////////printk(LOG_LEVEL "~~current chunkNo: %d~~~ now previous %d~~~~ \n", chunkNo, filp->f_dentry->d_inode->i_version);

	return;
}

static void write_to_redis(unsigned long inode_no, unsigned char* buf, unsigned len, unsigned file_size, unsigned char* file_path)
{
	redisReply *reply;
	char key[500];
	int fd = 0;
	//int c_id = 0;
	/*aux var for writing */
	int chunkNo = 0;
	int write_len = 0;
	int to_write = 0;
	int progress = 0;
	int offset = 0;
	char *aux_buf =  (char *) kmalloc(sizeof(char)*CHUNK, GFP_KERNEL);
	memset(aux_buf, 0, sizeof(char) * CHUNK);
	/*identify on which redis server is the file placed*/
	//int r_id = identify_server(file_path);
	/*identify the connection to the redis server*/
	//if((c_id = identify_client(inode_no, r_id)) == -1){
		//////printk("ERR: Wrong file descriptor %d\n", fd);
	//}
	//fd = redis[r_id].fd[c_id];

	//printk(LOG_LEVEL "***************************WRITE***************************\n");
	//printk(LOG_LEVEL "file:%s\n", file_path);
	//////printk(LOG_LEVEL "what to write %s \n", buf);
	//////printk(LOG_LEVEL "buf len %d \n", len);
	

	//////printk(LOG_LEVEL "CURRENT SIZE: %d \n~~~~~~~~\n", file_size);

//mutex_lock(&processpid);
	chunkNo = file_size / CHUNK + 1;
	offset  = file_size % CHUNK;
	to_write = CHUNK - offset; //how much do i have to write for the first time

	/* if we do not have that much to write*/
	if(to_write > len){
		to_write = len;
	}
	
	//////printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	write_len = len;
	while(write_len > 0){
		/* create the key and write to redis */
		//sprintf(key, "%s_chunk%d", redis[r_id].f_name[c_id], chunkNo);
		sprintf(key, "%s_chunk%d", file_path, chunkNo);
		//printk(LOG_LEVEL "WRITE key:%s\n", key);

		/*identify each individual chunk on its assigned redis
 * 		 * server*/
		int ch_r_id = identify_server(key);
		int ch_c_id = get_free_channel(ch_r_id);
	
		if(redis[ch_r_id].sock[ch_c_id]){
			////printk(LOG_LEVEL "socket not null%d\n",redis.sock[c_id]);
		}
		redis[ch_r_id].fd[ch_c_id] = sock_map_fd(redis[ch_r_id].sock[ch_c_id], 0);
		fd = redis[ch_r_id].fd[ch_c_id];
	
		//////printk(LOG_LEVEL "key: %s\n", key);
		//////printk(LOG_LEVEL "to_write %d\n", to_write);
		//////printk(LOG_LEVEL "progress: %d\n", progress);

		memcpy(aux_buf, buf + progress, to_write);
		aux_buf[to_write] = '\0';
		//////printk(LOG_LEVEL "Wha is writing to Redis: aux_buf(value): %s\n", aux_buf);

		//////printk(LOG_LEVEL "to_write: %d\n", to_write);
		/* write to redis */
		reply = redisCommand(fd, "APPEND  %b %b", key, strlen(key), aux_buf, to_write);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		/*clean data structures*/
		freeReplyObject(reply);
		aux_buf[0] = '\0';

		/* update offsets*/
		progress  += to_write;
		write_len -=to_write;
		//////printk(LOG_LEVEL "write_len %d\n", write_len);

		/*go to next chunk and compute the size*/
		chunkNo++;
		if(write_len / CHUNK > 0){
			to_write = CHUNK;
		}else{
			to_write = write_len;
		}

		//printk(LOG_LEVEL "server: %d client :%d\n",ch_r_id, ch_c_id);
		//release file descriptor
		sys_close(redis[ch_r_id].fd[ch_c_id]);
	
		//release communication channel to reids
		release_channel(ch_r_id, ch_c_id);
	}
	//////printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

	kfree(aux_buf);
	
//mutex_unlock(&processpid);
	return;
}


static unsigned get_file_size(long inode_no, int r_id){
	redisReply *reply;	
	unsigned f_size = 0;

	int c_id = 0;
	int fd = 0;
	
	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, SIZE, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, SIZE)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];


	////////printk(LOG_LEVEL "--------------------REDIS: GET FILE SIZE------------------\n");
	////////printk(LOG_LEVEL "name: %s \n", redis[r_id].f_name[c_id]);

	reply = redisCommand(fd, "GET %s ", redis[r_id].f_name[c_id]);
	/* if the file does not exist then the size should be 0*/
	if(reply->type != REDIS_REPLY_NIL){
		f_size = simple_strtol((const char*)reply->reply, NULL, 10);
 	}
 	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	freeReplyObject(reply);
	
	
	return f_size;
}


static void update_file_size(long inode_no, unsigned size, int r_id){
	redisReply *reply;
	char file_size[100];
	
	int c_id = 0;
	int fd = 0;
	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, SIZE, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, SIZE)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];

	////////printk(LOG_LEVEL "--------------------REDIS: UPDATE FILE SIZE------------------\n");
	////////printk(LOG_LEVEL "name: %s   size:%d\n", name, size);


	sprintf(file_size, "%d", size);
	reply = redisCommand(fd, "SET %s %s", redis[r_id].f_name[c_id], file_size);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	freeReplyObject(reply);
	////////printk(LOG_LEVEL "reply for size: %s \n", reply->reply);

	return;
}
/**
 * Add a dir to its parent. Create the parent if not present
 */
static void add_dir_entry(long inode_no, struct dentry* dentry, int r_id){
	redisReply *reply;

	unsigned char parent[500];
	unsigned char name[500]; //dir or file name

	int c_id = 0;
	int fd = 0;

	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DIROPS, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DIROPS)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];
	
	/*get the full path of the parent -> key in redis*/
	get_full_path_of_file(dentry->d_parent, parent);
	
	/*create the value represented by the file or dir*/
	if (S_ISDIR(dentry->d_inode->i_mode)) {/* if dir */
		//sprintf(name, "%s/", dentry->d_iname);
		sprintf(name, "%s/", dentry->d_name.name);
	}else{/* if file*/
		//sprintf(name, "%s", dentry->d_iname);
		sprintf(name, "%s", dentry->d_name.name);
	}

	////////printk(LOG_LEVEL "parent:%s name:%s\n", parent, name);

	reply = redisCommand(fd, "LPUSH %s %s", parent, name);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);

	/* remove this entry from the marked as deleted list which has
	 * as key 'dir/' + '#'*/
	reply = redisCommand(fd, "LREM %s/#  0 %s", parent, name);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);

	freeReplyObject(reply);
	
	return;
}

static void remove_dir_entry(long inode_no, struct dentry* dentry, int r_id){
	redisReply *reply;

	unsigned char parent[500];
	unsigned char name[500]; //dir or file name

	int c_id = 0;
	int fd = 0;
	
	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DIROPS, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DIROPS)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];
	
	/*get the full path of the parent -> key in redis*/
	get_full_path_of_file(dentry->d_parent, parent);
	
	/*create the value represented by the file or dir*/
	if (S_ISDIR(dentry->d_inode->i_mode)) {/* if dir */
		//sprintf(name, "%s/", dentry->d_iname);
		sprintf(name, "%s/", dentry->d_name.name);
	}else{/* if file*/
		//sprintf(name, "%s", dentry->d_iname);
		sprintf(name, "%s", dentry->d_name.name);
	}

	////////printk(LOG_LEVEL "parent:%s name:%s\n", parent, name);

	reply = redisCommand(fd, "LREM %s  0 %s", parent, name);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
	/*this check is not needed because this function is being called
	 * after lookup was successful. Just to have a clean
	 * implementation we check for it.*/
	if(reply->integer == 1){
		/*mark the entry as removed from the redis*/
		reply = redisCommand(fd, "LPUSH %s/#  %s", parent, name);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	}

	freeReplyObject(reply);

	return;
}
/**
 * get the entries of a directory
 * if isDelDir == 1 -> return entries that have been deleted
 */
static redisReply*  get_dir_entry(long inode_no, struct dentry *dentry, int isDelDir, int r_id){
	redisReply *reply;
	unsigned char dir_path[500];

	int c_id = 0;
	int fd = 0;
	
	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DIROPS, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DIROPS)) == -1){
		////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];
	
	////////printk(LOG_LEVEL "--------------------------GET DIR ENTRY-----------------\n");
	/*get the full path of the current dir -> key in redis*/
	get_full_path_of_file(dentry, dir_path);

	//sprintf(name, "%s", dentry->d_iname);
	////////printk(LOG_LEVEL "name:%s\n", dir_path);

	/* if we want to get the contents of delete files/dirs */
	if(isDelDir == 1){
		sprintf(dir_path, "%s/#", dir_path);
		////////printk(LOG_LEVEL "name:%s\n", dir_path);
	}

	reply = redisCommand(fd, "LRANGE %s  0 -1 ", dir_path);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
	return reply;
}

static int dir_entry_contains(long inode_no, struct dentry *dentry, unsigned char* name, int r_id){
	redisReply *reply;
	unsigned char dir_path[500];

	int exists = 0;
	int c_id = 0;
	int fd = 0;

	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DIROPS, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DIROPS)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];
	
	/*get the full path of the current dir -> key in redis*/
	get_full_path_of_file(dentry, dir_path);

	////////printk(LOG_LEVEL "name:%s\n", dir_path);

	/*check first to see if there is a directory called 'name'*/
	reply = redisCommand(fd, "LREM %s  0 %s/", dir_path, name);
	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
	if(reply->integer == 1){
		//add the dir again to redis
		reply = redisCommand(fd, "LPUSH %s %s/", dir_path, name);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		
		exists = 1; //directory exsists
	}else { //directory not found

		/*search for the file with the exact 'name' without '/'*/
		reply = redisCommand(fd, "LREM %s  0 %s", dir_path, name);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
		if(reply->integer == 1 ){
			//add the file again to redis
			reply = redisCommand(fd, "LPUSH %s %s", dir_path, name);
			assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		
			exists = 2; //file exists
		}
	}
	
	freeReplyObject(reply);
	
	return exists;
}

static int dir_del_entry_contains(long inode_no, struct dentry *dentry, unsigned char* name, int r_id){
	redisReply *reply;
	unsigned char dir_path[500];

	int exists = 0;
	int c_id = 0;
	int fd = 0;

	//mutex_lock(&processpid);
	//int pid = task_pid_nr(current);
	//if((c_id = identify_client(inode_no, r_id, DIROPS, pid)) == -1){
	if((c_id = identify_client(inode_no, r_id, DIROPS)) == -1){
		//////////////////////////printk("ERR: Wrong file descriptor %d\n", fd);
	}
	//mutex_unlock(&processpid);
	fd = redis[r_id].fd[c_id];
	
	/*get the full path of the current dir -> key in redis*/
	get_full_path_of_file(dentry->d_parent, dir_path);

	////////printk(LOG_LEVEL "parent dir:%s\n", dir_path);
	////////printk(LOG_LEVEL "rights:%d\n", dentry->d_inode->i_mode);
	
	if(S_ISDIR(dentry->d_inode->i_mode)){ //if is a dir
		////////printk(LOG_LEVEL "IS A DIRR \n ");
		/*check first to see if there is dir  called 'name'*/
		reply = redisCommand(fd, "LREM %s/#  0 %s/", dir_path, name);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
		if(reply->integer == 1){
			//add the dir again to redis
			reply = redisCommand(fd, "LPUSH %s/# %s/", dir_path, name);
			assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		
			exists = 1; //dir exsists
			freeReplyObject(reply);
		}
	}else if (S_ISREG(dentry->d_inode->i_mode)){
		////////printk(LOG_LEVEL "IS A FILEEEE \n ");
		/*check first to see if there is a file called 'name'*/
		reply = redisCommand(fd, "LREM %s/#  0 %s", dir_path, name);
		assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
	
		if(reply->integer == 1){
			//add the dir again to redis
			reply = redisCommand(fd, "LPUSH %s/# %s", dir_path, name);
			assert(reply->type == REDIS_REPLY_ERROR, reply->reply);
		
			exists = 2; //file exists
			freeReplyObject(reply);
		}
	}
	
	return exists;
}

/**
 * Map to local file system the directories and files.
 * and remove inodes/dentry that have been deleted from remote
 */
static void map_dir_entries_locally(long inode_no, struct dentry *parent){
	
	redisReply *entries;
	redisReply *entries_del;
	int j = 0;
	/* name of the file, dir*/
	char name[500];
	umode_t mode;

	char parent_path[500];
	get_full_path_of_file(parent, parent_path);

	int p_r_id = identify_server(parent_path);
	int p_c_id = get_free_channel(p_r_id);
	
	////////printk(LOG_LEVEL "--------------------------MAP LOCALLY-----------------\n");
	if(redis[p_r_id].sock[p_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);

	/*taint file for DIR ops*/
	redis[p_r_id].taint[p_c_id] = DIROPS;
	redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
	// assign local inode number of the parent
	redis[p_r_id].inode_no[p_c_id] = inode_no;

	// get  directory entries from redis
	entries = get_dir_entry(inode_no, parent, 0, p_r_id);

	for( j = 0; j < entries->elements; j++){
		////////printk(LOG_LEVEL "FROM REDIS: %s \n",entries->element[j]->reply);
		
		/*decide if a file or dir and then create the name and
		 * assign permissions*/
		if(strchr(entries->element[j]->reply, '/') != NULL){
			/*create the dir name without '/'*/
			snprintf(name, strlen(entries->element[j]->reply), "%s", entries->element[j]->reply);
			mode = S_IFDIR | 0777;//0644;
		}else{
			sprintf(name, "%s",  entries->element[j]->reply);
			mode = S_IFREG | 0777;//0644;
		}

		////////printk(LOG_LEVEL "NAME: %s \n", name);

		struct dentry* d;
		struct inode *inode;

		//lookup
		struct qstr qname;
		qname.name = name;
		qname.len = strlen(name);
		qname.hash = full_name_hash(qname.name, qname.len);
		
		struct dentry *find = NULL;
		/* search and see if the current dir contains specified file/dir
		 * Efficient search: checking only the hash between name and len
		 * */
		find = d_lookup(parent, &qname);
		if(!find){
			//create dir or file
			d = d_alloc_name(parent, name);
			inode = memFS_get_inode(parent->d_inode->i_sb, mode);
			d_add(d, inode);

			/*update the size of the file-> take it from redis*/
			if(S_ISREG(mode)){
				char file_path[500];
				get_full_path_of_file(d, file_path);
				////////printk(LOG_LEVEL "full path new size: %s\n", full_path);
				int f_r_id = identify_server(file_path);
				int f_c_id = get_free_channel(f_r_id);

				redis[f_r_id].fd[f_c_id] = sock_map_fd(redis[f_r_id].sock[f_c_id], 0);

				/* copy the name of the file*/
				sprintf(redis[f_r_id].f_name[f_c_id], "%s", file_path);

				/*taint for file SIZE ops*/
				redis[f_r_id].taint[f_c_id] = SIZE;
				redis[f_r_id].pid[f_c_id] = task_pid_nr(current);

				/* assign local inode number and
				 * overwrite the eralier one(parent)*/
				redis[f_r_id].inode_no[f_c_id] = inode->i_ino;

				/* get the size of the file from redis*/
				redis[f_r_id].f_size[f_c_id] =  get_file_size(redis[f_r_id].inode_no[f_c_id], f_r_id);

				/*set to local file system the size*/
				int error = simple_setsize(inode, redis[f_r_id].f_size[f_c_id]);
				if (error){
					////////printk(LOG_LEVEL "Error on setting size for file: %s\n", name);
				}
		
				//release file descriptor
				sys_close(redis[f_r_id].fd[f_c_id]);
	
				//release communication channel to reids
				release_channel(f_r_id, f_c_id);
			}
		}
		else{
			////////printk(LOG_LEVEL "found it\n");
			dput(find);
		}

		name[0] ='\0';

	}
	//free objects
	//freeReplyObject(entries);

	// reassign local inode number of the parent
	redis[p_r_id].inode_no[p_c_id] = inode_no;

	/*get dir entries for deleted files*/
	entries = get_dir_entry(inode_no, parent, 1, p_r_id);
	
	for( j = 0; j < entries->elements; j++){

		if(strchr(entries->element[j]->reply, '/') != NULL){
			/*create the dir name without '/'*/
			snprintf(name, strlen(entries->element[j]->reply), "%s", entries->element[j]->reply);
		}else{
			sprintf(name, "%s",  entries->element[j]->reply);
		}
		////////printk(LOG_LEVEL "READDIR DEL NAME: %s \n", name);


		//lookup
		struct qstr qname;
		qname.name = name;
		qname.len = strlen(name);
		qname.hash = full_name_hash(qname.name, qname.len);
		
		struct dentry *find = NULL;
		/* search and see if the current dir still contains specified file/dir
		 * Efficient search: checking only the hash between name and len
		*/
		find = d_lookup(parent, &qname);
		if(find){
			////////printk(LOG_LEVEL "------------READDIR DEL: FIND IT \n");
			/* remove file metadata and return error*/
			/* if there is an inode number assoicated for
			 * with the parent then we can delete it,
			 * otherwise an earlier lookup assoicated this
			 * dentry with NULL*/
			if(find_inode_number(parent, &qname) != 0){
				////////printk(LOG_LEVEL "d_validate is passed//// \n");
				simple_unlink(parent->d_inode, find);
				d_delete(find);
			}
			dput(find);
		}else{
			dput(find);
		}

		name[0] ='\0';

	}
	//release file descriptor
	sys_close(redis[p_r_id].fd[p_c_id]);
	
	//release communication channel to reids
	release_channel(p_r_id, p_c_id);
	
	//free object
	freeReplyObject(entries);

	return;
}
/****************** Redis communication ************/
/**
 * When using a 32bit arch I have encountered some preoblems realted to
 * generating an inode number, therefore this function which is 
 * defined in the linux kernel avoids that problem.
 *
 * On a 32bit, non LFS stat() call, glibc will generate an EOVERFLOW
 * error if st_ino won't fit in target struct field. Use 32bit counter
 * here to attempt to avoid that.
 */
unsigned int get_next_ino(void)
{
         unsigned int *p = &get_cpu_var(last_ino);
         unsigned int res = *p;

#ifdef CONFIG_SMP
        if (unlikely((res & (LAST_INO_BATCH-1)) == 0)) {
                static atomic_t shared_last_ino;
                 int next = atomic_add_return(LAST_INO_BATCH, &shared_last_ino);

                res = next - LAST_INO_BATCH;
         }
#endif

        *p = ++res;
        put_cpu_var(last_ino);
        return res;
}

struct inode *memFS_get_inode(struct super_block *sb, int mode)
{
	struct inode *inode = new_inode(sb);
	if (inode->i_ino == 0){
		inode->i_ino = get_next_ino();
		////////printk(LOG_LEVEL "Inode :%d\n", inode->i_ino);
	}
	mode = mode | 0777;
	////////printk(LOG_LEVEL "----------------------GET INODE---------------\n");

	if (!inode)
		return NULL;

	inode->i_mode = mode;
	inode->i_uid = current_fsuid();
	inode->i_gid = current_fsgid();
	inode->i_blocks = 0;
	inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;

	/* if dir */
	if (S_ISDIR(mode)) {
		/* can leave file_operations to simple VFS
		 * implementation */
		/* directory inodes start off with i_nlink == 2 (for "." entry)
		 * */
		////////////////////////printk("######IS A DIRECTORY####### \n");
		inode->i_op = &memFS_dir_inode_operations;
		inode->i_fop = &memFS_dir_operations;
		inc_nlink(inode);
	}
	
	/* if regular file */
	if (S_ISREG(mode)) {
		/* set flags for handling operations on files */
		////////////////////////printk("######IS A FILE####### \n");
		inode->i_op = &memFS_file_inode_operations;
		inode->i_fop = &memFS_file_operations;
		inode->i_mapping->a_ops = &memFS_aops;
	}
	return inode;
}

/*
 *  memFS_mknod, memFS_create, memFS_mkdir, memfs_setattr
 */

/*
 * File creation. Alloc inode
 */
static int memFS_mknod(struct inode *dir, struct dentry *dentry, umode_t mode, dev_t dev)
{
        struct inode * inode = memFS_get_inode(dir->i_sb, mode);
        int error = -ENOSPC; /* No space left on device */

	////////printk(LOG_LEVEL "*****************MKNOD*********************\n");
	if (inode) {
		d_instantiate(dentry, inode);

		dget(dentry);   /* Extra count - pin the dentry in core */
		error = 0;
		dir->i_mtime = dir->i_ctime = CURRENT_TIME;

		char parent_path[500];
		get_full_path_of_file(dentry->d_parent, parent_path);
		////////printk(LOG_LEVEL "MKNOD: %s   inode:%d\n", dentry->d_name.name, dentry->d_inode->i_ino);
		////////printk(LOG_LEVEL "full path mknod: %s\n", parent_path);
		
		////////printk(LOG_LEVEL "*****************CREATE DIR ENTRY*********************\n");
		int p_r_id = identify_server(parent_path);
		int p_c_id = get_free_channel(p_r_id);

		if(redis[p_r_id].sock[p_c_id]){
			////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
		}
		redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);
		
		/*taint file for DIR ops*/
		redis[p_r_id].taint[p_c_id] = DIROPS;
		redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
		// assign local inode number
		redis[p_r_id].inode_no[p_c_id] = dentry->d_inode->i_ino;

		// add the directory to redis
		add_dir_entry(redis[p_r_id].inode_no[p_c_id], dentry, p_r_id);

		//release file descriptor
		sys_close(redis[p_r_id].fd[p_c_id]);

		//release communication channel to reids
		release_channel(p_r_id, p_c_id);

        }
        return error;
}

/*
 * creates a new inode. It is called when the sys calls create and open
 * are called
 */
static int memFS_create(struct inode *dir, struct dentry *dentry, umode_t mode, bool excl)
{

	////////printk(LOG_LEVEL "*****************CREATE*********************\n");
        return memFS_mknod(dir, dentry, mode | S_IFREG, 0);
}


/*
 * Create a new dir. It is called when mkdir is called
 * First we call memFS_create, alloc a new data block and create "." and
 * ".."
 */
static int memFS_mkdir(struct inode * dir, struct dentry * dentry, umode_t mode)
{
	////////printk(LOG_LEVEL "*****************MKDIR*********************\n");
	int retval = memFS_mknod(dir, dentry, mode | S_IFDIR, 0);
        if (!retval)
                inc_nlink(dir);
	
	return retval;
}


static int memfs_unlink(struct inode *dir, struct dentry *dentry)
{
	struct inode *inode = dentry->d_inode;

	char parent_path[500];
	char file_path[500];
	get_full_path_of_file(dentry->d_parent, parent_path);
	get_full_path_of_file(dentry, file_path);
	
	/* identify parent and child on redis server*/
	int p_r_id = identify_server(parent_path);
	int f_r_id = identify_server(file_path);

	/* get a slot for parent and child*/
	int p_c_id = get_free_channel(p_r_id);
	int f_c_id = get_free_channel(f_r_id);

	////////printk(LOG_LEVEL "*****************DELETE*********************\n");
	/**
	 * remove file from redis and release channel
	 */
	if(redis[f_r_id].sock[f_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[f_r_id].fd[f_c_id] = sock_map_fd(redis[f_r_id].sock[f_c_id], 0);

	////////printk(LOG_LEVEL "full path: %s\n", full_path);

	/* copy the name if the file*/
	sprintf(redis[f_r_id].f_name[f_c_id], "%s", file_path);
	/* assign local inode number*/
	redis[f_r_id].inode_no[f_c_id] = dentry->d_inode->i_ino;
	/*taint for file SIZE ops*/
	redis[f_r_id].taint[f_c_id] = SIZE;
	redis[f_r_id].pid[f_c_id] = task_pid_nr(current);
	/* get the size of the file from redis*/
	redis[f_r_id].f_size[f_c_id] =  get_file_size(redis[f_r_id].inode_no[f_c_id], f_r_id);
	/*taint for DELETE*/
	redis[f_r_id].taint[f_c_id] = DELETE;
	redis[f_r_id].pid[f_c_id] = task_pid_nr(current);
	/*remove file from redis*/
	remove_file_from_redis(redis[f_r_id].inode_no[f_c_id], redis[f_r_id].f_size[f_c_id], f_r_id);
	//release file descriptor
	sys_close(redis[f_r_id].fd[f_c_id]);
	//release communication channel to reids
	release_channel(f_r_id, f_c_id);

	/**
	 * remove directory entry from redis
	 */
	if(redis[p_r_id].sock[p_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);
	/*taint file for DIR ops*/
	redis[p_r_id].taint[p_c_id] = DIROPS;
	redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
	/*assign inode of file*/
	redis[p_r_id].inode_no[p_c_id] = dentry->d_inode->i_ino;
	/*remove file entry from redis*/
	remove_dir_entry(redis[p_r_id].inode_no[p_c_id], dentry, p_r_id);
	//release file descriptor
	sys_close(redis[p_r_id].fd[p_c_id]);
	//release communication channel to reids
	release_channel(p_r_id, p_c_id);


	inode->i_ctime = dir->i_ctime = dir->i_mtime = CURRENT_TIME;
	drop_nlink(inode);
	dput(dentry);
	return 0;
}

/**
 * simple_setsize - handle core mm and vfs requirements for file size change
 * @inode: inode
 * @newsize: new file size
 *
 * Returns 0 on success, -error on failure.
 *
 * simple_setsize must be called with inode_mutex held.
 *
 * simple_setsize will check that the requested new size is OK (see
 * inode_newsize_ok), and then will perform the necessary i_size update
 * and pagecache truncation (if necessary). It will be typically be called
 * from the filesystem's setattr function when ATTR_SIZE is passed in.
 *
 * The inode itself must have correct permissions and attributes to allow
 * i_size to be changed, this function then just checks that the new size
 * requested is valid.
*/
int simple_setsize(struct inode *inode, loff_t newsize)
{
	loff_t oldsize;
	int error;

	error = inode_newsize_ok(inode, newsize);
        if (error)
		return error;
        oldsize = inode->i_size;
        i_size_write(inode, newsize);
        truncate_pagecache(inode, oldsize, newsize);

        return error;
}

/**
 * generic_setattr - copy simple metadata updates into the
 * generic inode
 * @inode:      the inode to be updated
 * @attr:       the new attributes
 *
 * generic_setattr must be called with i_mutex held.
 *
 * generic_setattr updates the inode's metadata with that specified
 * in attr. Noticably missing is inode size update, which is more
 * complex as it requires pagecache updates. See simple_setsize.
 *
 * The inode is not marked as dirty after this operation. The
 * rationale is  that for "simple" filesystems, the struct inode 
 * is the inode  storage.
 * The caller is free to mark the inode dirty afterwards if needed.
 */
void generic_setattr(struct inode *inode, const struct iattr *attr)
{
	unsigned int ia_valid = attr->ia_valid;

	if (ia_valid & ATTR_UID)
		inode->i_uid = attr->ia_uid;
	if (ia_valid & ATTR_GID)
		inode->i_gid = attr->ia_gid;
	if (ia_valid & ATTR_ATIME)
		inode->i_atime = timespec_trunc(attr->ia_atime,
				inode->i_sb->s_time_gran);
	if (ia_valid & ATTR_MTIME)
		inode->i_mtime = timespec_trunc(attr->ia_mtime,
				inode->i_sb->s_time_gran);
	if (ia_valid & ATTR_CTIME)
		inode->i_ctime = timespec_trunc(attr->ia_ctime,
				inode->i_sb->s_time_gran);
	if (ia_valid & ATTR_MODE) {
		umode_t mode = attr->ia_mode;

		if (!in_group_p(inode->i_gid) && !capable(CAP_FSETID))
			mode &= ~S_ISGID;
		inode->i_mode = mode;
	}
}

/**
 * simple_setattr - setattr for simple in-memory filesystem
 * @dentry: dentry
 * @iattr: iattr structure
 *
 * Returns 0 on success, -error on failure.
 *
 * simple_setattr implements setattr for an in-memory filesystem
 * which  does not store its own file data or metadata (eg. uses 
 * the page cachei and inode cache as its data store).
 *
 * source: http://lxr.free-electrons.com/source/fs/libfs.c?v=2.6.35#L355
 */
int memfs_setattr(struct dentry *dentry, struct iattr *iattr)
{
	struct inode *inode = dentry->d_inode;
	int error;

        error = inode_change_ok(inode, iattr);
        if (error)
		return error;


	/* truncate the size of the file to 0, update  data structure accordingly*/
	if (iattr->ia_valid & ATTR_SIZE) {
		
		////////printk(LOG_LEVEL "**************TRUNCATE***************\n");
		
		char full_path[200];
		get_full_path_of_file(dentry, full_path);
		
		int r_id = identify_server(full_path);
		int c_id = get_free_channel(r_id);
	
		if(redis[r_id].sock[c_id]){
			////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
		}
		redis[r_id].fd[c_id] = sock_map_fd(redis[r_id].sock[c_id], 0);
	
		////////printk(LOG_LEVEL "full path: %s\n", full_path);

		/* copy the name if the file*/
		sprintf(redis[r_id].f_name[c_id], "%s", full_path);
		/* get local value of inode no*/
		redis[r_id].inode_no[c_id] = dentry->d_inode->i_ino;
		/*taint file for SIZE ops */
		redis[r_id].taint[c_id] = SIZE;
		redis[r_id].pid[c_id] = task_pid_nr(current);
		/* get the size of the file from redis*/
		redis[r_id].f_size[c_id] =  get_file_size(redis[r_id].inode_no[c_id], r_id);
		/*taint file for DELETE*/
		redis[r_id].taint[c_id] = DELETE;
		redis[r_id].pid[c_id] = task_pid_nr(current);
		/*remove file from redis*/
		remove_file_from_redis(redis[r_id].inode_no[c_id], redis[r_id].f_size[c_id], r_id);
		//release file descriptor
		sys_close(redis[r_id].fd[c_id]);
		//release communication channel to reids
		release_channel(r_id, c_id);

		
		error = simple_setsize(inode, iattr->ia_size);
		if (error)
			return error;
		
	}

	generic_setattr(inode, iattr);
	return error;
}

static int memfs_getattr(struct vfsmount *mnt, struct dentry *dentry, struct kstat *stat)
{
	struct inode *inode = dentry->d_inode;
	
	////////printk(LOG_LEVEL "*****************GETATTR***************\n");
	
	generic_fillattr(inode, stat);
	stat->blocks = inode->i_mapping->nrpages << (PAGE_CACHE_SHIFT - 9);
	return 0;
}

/*
 *reverse a string in place
 * ex: hello -> olleh
 */
static void reverse_string(char str[], int start, int end){
	while(end > start){
		char aux = str[start];
		str[start] = str[end];
		str[end] = aux;
		start++; end--;
	}
	return;
}
/*
 * reverse a sentece in place
 * ex: hello world -> world hello
 */
static void reverse_sentence(char str[]){
	int len = 0, end = 0;
	if (str == NULL){
		return;
	}

	len = strlen(str);
	reverse_string(str, 0, len-1);
	end = 0;

	while(len > end){
		if(str[end]  != '/'){

			int start = end;
			while(str[end] != '/' && len > end){
				end++;
			}

			//back up to the end of the word
			end--;

			reverse_string(str,start,end);
		}
		end++;//advance to the next token
	}
	return;
}

/*
 *Get the full path of a file by going in depth until reach the '/'
 */
static void get_full_path_of_file(struct dentry *dentry, char full_path[])
{
	struct dentry *parent = dentry->d_parent;
	char format[5] ="%s";
	int len = 0;

	//if(strcmp((unsigned char*)dentry->d_iname, (unsigned char*)"/") != 0){
	if(strcmp((unsigned char*)dentry->d_name.name, (unsigned char*)"/") != 0){
		strcat(format, "/");
	}

	//sprintf(full_path, format, dentry->d_iname);	
	sprintf(full_path, format, dentry->d_name.name);	
	while(strcmp((unsigned char*)parent->d_iname, (unsigned char*)"/") != 0){
		strcat(full_path, parent->d_iname);
		strcat(full_path, "/");

		////////printk(LOG_LEVEL " %s\n", parent->d_iname);
		parent = parent->d_parent;
	}
	//here we have file1/test/ we want:/test/file1
	reverse_sentence(full_path);
	
	len = strlen(full_path);
	full_path[len]='\0';
	////////printk(LOG_LEVEL "PATH: %s\n", full_path);
	return;
}

/**
 * Called when a file is Open.
 * START OF CLIENT JOURNEY
 */
static int memfs_open(struct inode *inode, struct file *file)
{
	char parent_path[500];//get full path of the file
	get_full_path_of_file(file->f_dentry->d_parent, parent_path);
	
	int p_r_id = identify_server(parent_path);
	int p_c_id = get_free_channel(p_r_id);
	
	////printk(LOG_LEVEL "------------------OPEN FILE--------------------\n");
	////////printk(LOG_LEVEL "PARENT: %s\n",parent_path);
	////////printk(LOG_LEVEL "name inode: %s\n", file->f_dentry->d_name.name);
	
	/* assign a file descriptor to the current client */
	if(redis[p_r_id].sock[p_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);
	//////////////////////////printk("~~~~FD: %d ~~~~\n", redis.fd[c_id]);
	/* copy the name if the file*/
	sprintf(redis[p_r_id].f_name[p_c_id], "%s", parent_path);	
	/*taint file for DIR ops*/
	redis[p_r_id].taint[p_c_id] = DIROPS;
	redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
	/* get the local inode number*/
	redis[p_r_id].inode_no[p_c_id] = file->f_dentry->d_parent->d_inode->i_ino;


	//	int i = 0;
	//	for (i =0; i < 8; i++){
			////////printk(LOG_LEVEL "PARENT :inode %d,  position:%d,  stored: %s    key:%s , taint  %d \n",redis[p_r_id].inode_no[i], i, redis[p_r_id].f_name[i], parent_path,redis[p_r_id].taint[i]) ;
	//	}
	


	/* if file was removed from redis by remote client
	 * then deallocate local metadata for file. For this I 
	 * had to compute the hash of the parent of the file since I 
	 * have to remove its entry from the redis*/
	//if(dir_del_entry_contains(redis[p_r_id].inode_no[p_c_id], file->f_dentry, file->f_dentry->d_iname, p_r_id) == 2){
	if(dir_del_entry_contains(redis[p_r_id].inode_no[p_c_id], file->f_dentry, file->f_dentry->d_name.name, p_r_id) == 2){
		////////printk(LOG_LEVEL "file '%s' found in DEL key on REDIS \n", file->f_dentry->d_iname);
		////////printk(LOG_LEVEL "parent: %s\n", file->f_dentry->d_parent->d_iname);
		//lookup
		struct qstr qname;
		//qname.name = file->f_dentry->d_iname;
		qname.name = file->f_dentry->d_name.name;
		qname.len = strlen(file->f_dentry->d_name.name);
		//qname.len = strlen(file->f_dentry->d_iname);
		qname.hash = full_name_hash(qname.name, qname.len);
		
		struct dentry *find = NULL;
		/* search and see if the current dir still contains specified file/dir
		 * Efficient search: checking only the hash between name and len
		*/
		find = d_lookup(file->f_dentry->d_parent, &qname);
		if(find){
			////////printk(LOG_LEVEL "------------OPEN: FIND IT \n");
			/* remove file metadata and return error*/
			simple_unlink(file->f_dentry->d_parent->d_inode, file->f_dentry);
			dput(find);
			d_delete(file->f_dentry);
		}else{
			dput(find);
		}

		//release file descriptor
		sys_close(redis[p_r_id].fd[p_c_id]);

		//release communication channel to reids
		release_channel(p_r_id, p_c_id);
		
		/* Err returned: Not such file or directory*/
		return ERR_PTR(-ENOENT);
	}else{
		//release file descriptor
		sys_close(redis[p_r_id].fd[p_c_id]);

		//release communication channel to reids
		release_channel(p_r_id, p_c_id);
	}

	////////printk(LOG_LEVEL "full path: %s\n", full_path);

	/**
	 *At this point we are certain that the file is still present in redis
	 * therefore we go on and open file normally
	 */
	unsigned size = 0;
	
	char file_path[500];//get full path of the parent of the file
	get_full_path_of_file(file->f_dentry, file_path);
	
	if(file->f_mode & FMODE_READ){
		////printk(LOG_LEVEL "------------------OPEN FILE----------------%s----\n", file_path);
		////printk(LOG_LEVEL "full path: %s\n", file_path);
		file->f_dentry->d_inode->i_version = 0;
	}


	////////printk(LOG_LEVEL "FILE: %s\n", file_path);
	int f_r_id = identify_server(file_path);
	int f_c_id = get_free_channel(f_r_id);
	
	/* assign a file descriptor to the current client */
	if(redis[f_r_id].sock[f_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[f_r_id].fd[f_c_id] = sock_map_fd(redis[f_r_id].sock[f_c_id], 0);
	/* copy the name if the file*/
	sprintf(redis[f_r_id].f_name[f_c_id], "%s", file_path);	
	/* get the local inode number*/
	redis[f_r_id].inode_no[f_c_id] = inode->i_ino;
	/*taint file for file SIZE ops*/
	redis[f_r_id].taint[f_c_id] = SIZE;
	redis[f_r_id].pid[f_c_id] = task_pid_nr(current);

	//	i = 0;
	//	for (i =0; i < 8; i++){
			////////printk(LOG_LEVEL "FILE O: inode %d,  position:%d,  stored: %s    key:%s , taint  %d \n",redis[f_r_id].inode_no[i], i, redis[f_r_id].f_name[i], file_path,redis[f_r_id].taint[i]) ;
	//	}
	




	/* check if file present in redis*/
	if( (size = get_file_size(redis[f_r_id].inode_no[f_c_id], f_r_id)) == 0){
		////////printk(LOG_LEVEL "------------------CREATE FILE in OPEN------------------\n");
		/* if size of the file is 0 then we assume is the first
		 * time accessed so create a file in redis with size 0*/
		update_file_size(redis[f_r_id].inode_no[f_c_id], 0, f_r_id);
	}

	////////printk(LOG_LEVEL "SIZE of file :'%s' is: %d\n", redis[f_r_id].f_name[f_c_id], size);
	/* else file already present, so "size" contains the actual size*/
	redis[f_r_id].f_size[f_c_id] = size;

	/*update the size of the file with the value from redis*/
	int error = simple_setsize(inode, redis[f_r_id].f_size[f_c_id]);
	if (error){
		////////printk(LOG_LEVEL "Error on setting size for file: %s\n", redis[f_r_id].f_name[f_c_id]);
	}


	//release file descriptor
	sys_close(redis[f_r_id].fd[f_c_id]);
	
	//release communication channel to reids
	release_channel(f_r_id, f_c_id);

	////////////////////////printk("^^^^^^^^file name :%s   file size: %d position:%d^^^^^^^\n", redis.f_name[c_id], redis.f_size[c_id], c_id);
	return 0;
}

/**
 * Called when afile is closed
 * END OF CLIENT JOURNEY
 */
static int memfs_release(struct inode *inode, struct file *file)
{
	struct ctrl_dbg *dbg = file->private_data;
	
	char key[500];
	char file_path[800];//get full path of the file
	get_full_path_of_file(file->f_dentry, file_path);



	mutex_lock(&processpid);
	//printk(LOG_LEVEL "-----------pid: %d--------RELEASE------------:%s-------- \n",task_pid_nr(current), file_path);
//	int chunkNo = inode->i_size / CHUNK + 1;
//	sprintf(key, "%s_chunk%d", file_path, chunkNo);
//	int ch_r_id = identify_server(key);

//	int ch_c_id = -1;
	//int pid = task_pid_nr(current);
	/* update the size of the file only in the WRITING mode*/
	//if ((file->f_mode & FMODE_WRITE) && ((ch_c_id=identify_client(-inode->i_ino, ch_r_id, WRITE)) != -1)){
	if ((file->f_mode & FMODE_WRITE) ){

		/**---------FLUSH DATA-------***/
		//int chunkNo = inode->i_size / CHUNK + 1;
		//sprintf(key, "%s_chunk%d", file_path, chunkNo);
		////////printk(LOG_LEVEL "W FILE %s \n", file_path);
		////////printk(LOG_LEVEL "W RELEASE:    INODE: %s \n", key);
		////////printk(LOG_LEVEL "RELEASE:    INODE neg: %d \n", inode->i_ino * (-1));
		
		/*identify echa individual chunk */
		//int ch_r_id = identify_server(key);
		//ch_c_id = identify_client(-inode->i_ino, ch_r_id, WRITE);
		////////printk(LOG_LEVEL "CHUNK ON RELEASE   %d   ch_c_id   %d \n", ch_r_id, ch_c_id);
		////////printk(LOG_LEVEL "INODE ON RELEASE   %d   in redis:  %d \n", inode->i_ino, redis[ch_r_id].inode_no[ch_c_id]);
		////////printk(LOG_LEVEL "BUFFER ON RELEASE    %d \n", redis[ch_r_id].buf_pos[ch_c_id]);
		
		/*if there is some data still in the buffer */
		////////printk(LOG_LEVEL "RELEASE buf_pos>0:    KEY: %s \n", key);
		////////printk(LOG_LEVEL "trece and buf_pos :%d \n", redis[ch_r_id].buf_pos[ch_c_id]);
		
		//printk(LOG_LEVEL "@@@@pid:%d@@@@  WRITE  before@@@:%s  @@@@@@:  server: %d  @@client:%d@@@@ \n", task_pid_nr(current), key, ch_r_id, ch_c_id);
		//printk(LOG_LEVEL " Size: %d \n", redis[ch_r_id].buf_pos[ch_c_id]);
		//printk(LOG_LEVEL " DAta: %s \n", redis[ch_r_id].buffer[ch_c_id]);

//	mm_segment_t old_fs = get_fs();
//	set_fs(KERNEL_DS);

		/* assign a file descriptor to the current client */
	//	if(redis[ch_r_id].sock[ch_c_id]){
			////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	//	}
	//	redis[ch_r_id].fd[ch_c_id] = sock_map_fd(redis[ch_r_id].sock[ch_c_id], 0);

	//	redisReply *reply;
	//	reply = redisCommand(redis[ch_r_id].fd[ch_c_id], 
	//				"APPEND  %b %b", 
	//				key, 
	///				strlen(key),
	///				redis[ch_r_id].buffer[ch_c_id],
	//				redis[ch_r_id].buf_pos[ch_c_id]);
			
	//	assert(reply->type == REDIS_REPLY_ERROR, reply->reply);

		/*clean data structures*/
	//	freeReplyObject(reply);

		//int i = 0;
		//for (i =0; i < 8; i++){
			////////printk(LOG_LEVEL "inode %d,  position:%d,  stored: %s    key:%s , taint  %d \n",redis[ch_r_id].inode_no[i], i, redis[ch_r_id].f_name[i], key,redis[ch_r_id].taint[i]) ;
		//}
		//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@  WRITE  @@@@@@@@@@@@@@@@@@@ \n");
	
		//release file descriptor
	//	sys_close(redis[ch_r_id].fd[ch_c_id]);
	//	//release communication channel to reids
	//	release_channel(ch_r_id, ch_c_id);
//	set_fs(old_fs);
	//	mutex_unlock(&processpid);
		////////printk(LOG_LEVEL "RELEASE:    KEY: %s \n", key);
		/*-------------update file size------------*/
		int f_r_id = identify_server(file_path);
		int f_c_id = get_free_channel(f_r_id);
	
		////////printk(LOG_LEVEL "Size FILE update on server %s \n",REDIS_SERVER_IP[f_r_id] );
		////////printk(LOG_LEVEL "FILE %s \n", file_path);
		////////printk(LOG_LEVEL "file size client :%d  inode: %d\n",inode->i_size ,  inode->i_ino);
		/* assign a file descriptor to the current client */
		if(redis[f_r_id].sock[f_c_id]){
			////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
		}
		redis[f_r_id].fd[f_c_id] = sock_map_fd(redis[f_r_id].sock[f_c_id], 0);
		/* copy the name if the file*/
		sprintf(redis[f_r_id].f_name[f_c_id], "%s", file_path);
		/*taint file for SIZE ops*/
		redis[f_r_id].taint[f_c_id] = SIZE;
		redis[f_r_id].pid[f_c_id] = task_pid_nr(current);
		/* get the local inode number*/
		redis[f_r_id].inode_no[f_c_id] = inode->i_ino;

		/* update the size of the file on closing on Redis*/
		update_file_size(redis[f_r_id].inode_no[f_c_id], inode->i_size, f_r_id);

		//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@  UPDATE SIZE  @@@@@@@@@@@@@@@@@@@ \n");
		//release file descriptor
		sys_close(redis[f_r_id].fd[f_c_id]);
		//release communication channel to reids
		release_channel(f_r_id, f_c_id);
		//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@  UPDATE SIZE2  @@@@@@@@@@@@@@@@@@@ \n");

	}
	//else{
	//	mutex_unlock(&processpid);
	//}

	//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@ PASSED RELEASE WRITE  @@@@@@@@@@@@@@@@@@@ \n");
	
	//chunkNo = file->f_pos / CHUNK + 1;
	//sprintf(key, "%s_chunk%d", file_path, chunkNo);
	//ch_r_id = identify_server(key);


	
//	mutex_lock(&processpid);
	//if ((file->f_mode & FMODE_READ) && (identify_client(-inode->i_ino, ch_r_id, READ) != -1)){
	if ((file->f_mode & FMODE_READ) ){
		int ch_c_id = -1;
		int size = inode->i_size;
		int ch = 0;
		
		//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@ RELEASE READ  @@@@@@@@@@@@@@@@@@@ \n");
		////printk(LOG_LEVEL "R size of file: %d  \n", size);
		for(ch = 1; ch <= size / CHUNK + 1; ch++){
			sprintf(key, "%s_chunk%d", file_path, ch);
			int ch_r_id = identify_server(key);
			ch_c_id = identify_client(-inode->i_ino, ch_r_id, READ);
			//printk(LOG_LEVEL "@@@@ RELEASE READ :%s @@@@@@@@server:%d @@@@client:%d @@@@@@@ \n", key, ch_r_id, ch_c_id);
			
			//mutex_lock(&processpid);
			//	int pid = task_pid_nr(current);
			//	if ((ch_c_id=identify_client(-inode->i_ino, ch_r_id, READ, pid)) != -1){
				if (ch_c_id != -1){
					////printk(LOG_LEVEL "R file %s \n", key);
					release_channel(ch_r_id, ch_c_id);
				}
		//	mutex_unlock(&processpid);
		}
		//char key[500];
	

		/*we need to identify the chunk that we left of,
		 * possibly not the eof, therefore we take current
		 * pointer in the file*/
		//int chunkNo = file->f_pos / CHUNK + 1;
		//sprintf(key, "%s_chunk%d", file_path, chunkNo);
		////////printk(LOG_LEVEL " prevoous chunk : %d  R RELEASE:    KEY: %s \n", key, file->f_dentry->d_inode->i_version);

		/*identify chunk (file) that we have to close*/
		//int ch_r_id = identify_server(key);
//		int ch_c_id = identify_client(-inode->i_ino, ch_r_id, READ);
	
		////////printk(LOG_LEVEL "R IDENTIFIED CLIENT for file :%s        server: %d  client: %d \n", key,ch_r_id,ch_c_id);
		//int i = 0;
		//for (i =0; i < 8; i++){
			////////printk(LOG_LEVEL "inode %d,  position:%d,  stored: %s    key:%s , taint  %d \n",redis[ch_r_id].inode_no[i], i, redis[ch_r_id].f_name[i], key,redis[ch_r_id].taint[i]) ;
		//}
		////////printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@@@@  READ  @@@@@@@@@@@@@@@@ \n");
	
		
		//release file descriptor
		//sys_close(redis[ch_r_id].fd[ch_c_id]);
		//release communication channel to reids
//		release_channel(ch_r_id, ch_c_id);
		file->f_dentry->d_inode->i_version = 0;
	}
	//printk(LOG_LEVEL "@@@@@@@@@@@@@@@@@ PASSED RELEASE READ  @@@@@@@@@@@@@@@@@@@ \n");
	mutex_unlock(&processpid);
	////////printk(LOG_LEVEL "######################RELEASE##############%s#########\n", file_path) ;
	kfree(dbg);
	//mutex_unlock(&file_release);
	return 0;
}

size_t memfs_do_sync_read(struct file *filp, char __user *buf, size_t len, loff_t *ppos)
{
        ssize_t ret = 0;
	unsigned copy_len = 0; 

	char file_path[200];
	get_full_path_of_file(filp->f_dentry, file_path);

	/* get the size of the file from inode datastructure */
	off_t file_size = filp->f_dentry->d_inode->i_size;

	////////printk(LOG_LEVEL "******************READ****************\n");
	////////printk(LOG_LEVEL "inode info i version: %ld\n", filp->f_dentry->d_inode->i_version);
	////////printk(LOG_LEVEL "inode info nlink: %ld\n", filp->f_dentry->d_inode->i_nlink);
	////////printk(LOG_LEVEL "len: %d\n", len
	////////printk(LOG_LEVEL "version: %lld\n", filp->f_version);

	/*compute the actual copy length */
	copy_len = min(file_size - min(file_size, (off_t)*ppos), (off_t)len);
	////////printk(LOG_LEVEL "~~~~~~~~~~~~~~~~copy_len: %d\n", copy_len);

	/*read from redis file*/
	if(copy_len > 0){
		//read_from_redis(filp->f_dentry->d_iname, file, (unsigned)*ppos, copy_len);

		//read_from_redis(filp->f_dentry->d_iname, (unsigned char*)buf, (unsigned)*ppos, copy_len);
		read_from_redis(filp, filp->f_dentry->d_inode->i_ino, (unsigned char*)buf, (unsigned)*ppos, copy_len, file_path);
		////////printk(LOG_LEVEL "memfs_do_sync_read2: %s", file);
	
		/* update position */
		*ppos += copy_len;
		ret    = copy_len;
	}
	
	//kfree(file);
	return ret;
}

ssize_t memfs_do_sync_write(struct file *filp, const char __user *buf, size_t len, loff_t *ppos)
{
	int ret;
	int error;
	
	char file_path[500];
	get_full_path_of_file(filp->f_dentry, file_path);


	/* get the size of the file from inode datastructure */
	off_t file_size = filp->f_dentry->d_inode->i_size;

	
	////////printk(LOG_LEVEL "##### file name: %s  #####\n", filp->f_dentry->d_iname );
	////////printk(LOG_LEVEL "##### size to write: %d  #####\n", len );
	////////printk(LOG_LEVEL "##### current file size: %ld  #####\n", file_size );
	
	
	//////////printk(LOG_LEVEL "##### what to write: %s  #####\n", file );
	//c_id = get_free_channel(r_id);
	//write_to_redis( filp->f_dentry->d_iname, file_write, len, file_size);
	write_to_redis(filp->f_dentry->d_inode->i_ino, (unsigned char*)buf, len, file_size, file_path);
//	write_to_redis(filp->f_dentry->d_iname, (unsigned char*)buf, len, file_size);

	
	/* update curernt position of the file */
	*ppos += len;

	/* update the size of the file*/
	file_size += len;
	
	/* update the size of the file in the inode data structure*/
	error = simple_setsize(filp->f_dentry->d_inode, file_size);
	if (error){
		ret = error;
		goto out;
	}

	ret = len;
out:
	return ret;

}

ssize_t memfs_read_dir(struct file *filp, char __user *buf, size_t siz, loff_t *ppos)
{
	////////printk(LOG_LEVEL "memfs read dir\n");
         return -EISDIR;
}

static inline unsigned char dt_type(struct inode *inode)
{
         return (inode->i_mode >> 12) & 15;
}

static inline int simple_positive(struct dentry *dentry)
{
	return dentry->d_inode && !d_unhashed(dentry);
}

static int simple_delete_dentry(struct dentry *dentry)
{
	return 1;
}

struct dentry *memfs_lookup(struct inode *dir, struct dentry *dentry, struct nameidata *nd)
{
	char parent_path[500];
	get_full_path_of_file(dentry->d_parent, parent_path);

	umode_t mode = 0;

	int p_r_id = identify_server(parent_path);
	int p_c_id = get_free_channel(p_r_id);

	static const struct dentry_operations simple_dentry_operations = {
		.d_delete = simple_delete_dentry,
	};

	if(redis[p_r_id].sock[p_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);
	/*taint file for DIR ops*/
	redis[p_r_id].taint[p_c_id] = DIROPS;
	redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
	// assign local inode number of the parent
	redis[p_r_id].inode_no[p_c_id] = dentry->d_parent->d_inode->i_ino;

	////////printk(LOG_LEVEL "-----------------LOOKUP--parent:%s------child:%s---flag:%d---------\n",dentry->d_parent->d_iname, dentry->d_name.name, dentry->d_flags);

	/*check if the file is in redis. if exists == 1(dir) else
	 * 2(file) 0(not found)*/
	//int exists = dir_entry_contains(dentry->d_parent->d_inode->i_ino, dentry->d_parent, dentry->d_iname, p_r_id);
	int exists = dir_entry_contains(dentry->d_parent->d_inode->i_ino, dentry->d_parent, dentry->d_name.name, p_r_id);
	if(exists == 1){ //dir found
		mode = S_IFDIR | 0777;//0644; //dir
	}else if(exists == 2) { // file found
		mode = S_IFREG | 0777; //0644; //file
	}else{
		if (dentry->d_name.len > NAME_MAX){
			return ERR_PTR(-ENAMETOOLONG);
		}
		dentry->d_op = &simple_dentry_operations;	
		d_add(dentry, NULL);
		////////printk(LOG_LEVEL "FILE / DIR NOT FOUND ^^^^^^^^^^^^^^^^^^^ \n");

		//release file descriptor
		sys_close(redis[p_r_id].fd[p_c_id]);
	
		//release communication channel to reids
		release_channel(p_r_id, p_c_id);
	
		return NULL;
	}

	/*release file descriptor of the parent of the current searched file*/
	sys_close(redis[p_r_id].fd[p_c_id]);
	//release communication channel to reids
	release_channel(p_r_id, p_c_id);
	
	
	/* if we reach this part that means we have to create either a
	 * file or a directory*/
	struct inode *inode = NULL;
	//unsigned char* name = dentry->d_iname;
	unsigned char* name = dentry->d_name.name;

	////////printk(LOG_LEVEL "redis contains file:%s\n ", name);

	/*create metadata for file or dir*/
	inode = memFS_get_inode(dir->i_sb, mode);
	dentry->d_op = &simple_dentry_operations;
	
	d_add(dentry, NULL);
	d_instantiate(dentry, inode);
	dget(dentry);
	dir->i_mtime = dir->i_ctime = CURRENT_TIME;

	/*update the size of the file-> take it from redis*/
	if(S_ISREG(mode)){
		char file_path[500];
		get_full_path_of_file(dentry, file_path);

		int f_r_id = identify_server(file_path);
		int f_c_id = get_free_channel(f_r_id);

		if(redis[f_r_id].sock[f_c_id]){
			////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
		}
		redis[f_r_id].fd[f_c_id] = sock_map_fd(redis[f_r_id].sock[f_c_id], 0);
		
		/* copy the name of the file*/
		sprintf(redis[f_r_id].f_name[f_c_id], "%s", file_path);

		/*taint file for file SIZE ops*/
		redis[f_r_id].taint[f_c_id] = SIZE;
		redis[f_r_id].pid[f_c_id] = task_pid_nr(current);
		/* assign local inode number and
		 * overwrite the eralier one(parent)*/
		redis[f_r_id].inode_no[f_c_id] = inode->i_ino;

		/* get the size of the file from redis*/
		redis[f_r_id].f_size[f_c_id] =  get_file_size(redis[f_r_id].inode_no[f_c_id], f_r_id);

		/*set to local file system the size*/
		int error = simple_setsize(inode, redis[f_r_id].f_size[f_c_id]);
		if (error){
			////////printk(LOG_LEVEL "Error on setting size for file: %s\n", name);
		}
		
		//release file descriptor
		sys_close(redis[f_r_id].fd[f_c_id]);
	
		//release communication channel to reids
		release_channel(f_r_id, f_c_id);
	}

	return NULL;
}


/*
 * Directory is locked and all positive dentries in it are safe, since
 * for ramfs-type trees they can't go away without unlink() or rmdir(),
 * both impossible due to the lock on directory.
*/
int memfs_readdir(struct file * filp, void * dirent, filldir_t filldir)
{
	struct dentry *dentry = filp->f_path.dentry;
	struct dentry *cursor = filp->private_data;
	struct list_head *p, *q = &cursor->d_u.d_child;
	ino_t ino;
	int i = filp->f_pos;
	
	
	////////printk(LOG_LEVEL "-----------------READDIR--------------------\n");
	switch (i) {
		case 0:
			ino = dentry->d_inode->i_ino;
			if (filldir(dirent, ".", 1, i, ino, DT_DIR) < 0)
				break;
			filp->f_pos++;
			i++;
			/* fallthrough */
		case 1:
			ino = parent_ino(dentry);
			if (filldir(dirent, "..", 2, i, ino, DT_DIR) < 0)
				break;
			filp->f_pos++;
			i++;
			/* fallthrough */
		default:
			/* map to local file system files and folders
			 * which are not present(been created remote)*/
			map_dir_entries_locally(filp->f_dentry->d_inode->i_ino, filp->f_dentry);

			spin_lock(&dentry->d_lock);
			if (filp->f_pos == 2)
				list_move(q, &dentry->d_subdirs);
			
			for (p=q->next; p != &dentry->d_subdirs; p=p->next) {
				struct dentry *next;
				next = list_entry(p, struct dentry, d_u.d_child);
				spin_lock_nested(&next->d_lock, DENTRY_D_LOCK_NESTED);
				if (!simple_positive(next)) {
					spin_unlock(&next->d_lock);
					continue;
				}
				
				spin_unlock(&next->d_lock);
				spin_unlock(&dentry->d_lock);
				if (filldir(dirent, next->d_name.name, 
							next->d_name.len, filp->f_pos, 
							next->d_inode->i_ino, 
							dt_type(next->d_inode)) < 0)
					return 0;
				spin_lock(&dentry->d_lock);
				spin_lock_nested(&next->d_lock, DENTRY_D_LOCK_NESTED);
				/* next is still alive */
				list_move(q, p);
				spin_unlock(&next->d_lock);
				p = q;
				filp->f_pos++;
			}
			spin_unlock(&dentry->d_lock);
	}
	return 0;
}

static int memfs_rmdir(struct inode *dir, struct dentry *dentry)
{
	char parent_path[500];
	get_full_path_of_file(dentry->d_parent, parent_path);
	
	int p_r_id = identify_server(parent_path);
	int p_c_id = get_free_channel(p_r_id);

	if (!simple_empty(dentry))
		return -ENOTEMPTY;
	
	////////printk(LOG_LEVEL "--------------------------RMDIR-------------------\n");

	/* remove dir entry from redis  */
	if(redis[p_r_id].sock[p_c_id]){
		////////printk(LOG_LEVEL "socket not null %d\n",redis.sock[c_id]);
	}
	redis[p_r_id].fd[p_c_id] = sock_map_fd(redis[p_r_id].sock[p_c_id], 0);

	/*taint file for DIR ops*/
	redis[p_r_id].taint[p_c_id] = DIROPS;
	redis[p_r_id].pid[p_c_id] = task_pid_nr(current);
	/* assign local inode number*/
	redis[p_r_id].inode_no[p_c_id] = dentry->d_inode->i_ino;

	/* remove the directory from redis*/
	remove_dir_entry(redis[p_r_id].inode_no[p_c_id], dentry, p_r_id);

	//release file descriptor
	sys_close(redis[p_r_id].fd[p_c_id]);

	//release communication channel to reids
	release_channel(p_r_id, p_c_id);

	drop_nlink(dentry->d_inode);
	simple_unlink(dir, dentry);
	drop_nlink(dir);
	
	return 0;
}
int simple_sync_file(struct file * file, struct dentry *dentry, int datasync)
{
	return 0;
}

/*
 * define structures (forward declarations)
 */
static const  struct address_space_operations memFS_aops = {
        .readpage       = simple_readpage,
        .write_begin    = simple_write_begin,
        .write_end      = simple_write_end,
};

static  struct inode_operations memFS_file_inode_operations = {
        .setattr        = memfs_setattr,
        .getattr        = memfs_getattr,
};

static const  struct file_operations memFS_file_operations = {
        .read           = memfs_do_sync_read,
        .aio_read       = generic_file_aio_read,
        .write          = memfs_do_sync_write,
        .aio_write      = generic_file_aio_write,
        .mmap           = generic_file_mmap,
        .fsync          = simple_sync_file,
        .splice_read    = generic_file_splice_read,
        .splice_write   = generic_file_splice_write,
        .llseek         = generic_file_llseek,
	.release        = memfs_release,
	.open		= memfs_open,
};
static struct inode_operations memFS_dir_inode_operations = {
	.create         = memFS_create,
        .lookup         = memfs_lookup,
	.link           = simple_link,
        .unlink         = memfs_unlink,
        .mkdir          = memFS_mkdir,
        .rmdir          = memfs_rmdir,
        .mknod          = memFS_mknod,
        .rename         = simple_rename,
};

static const struct file_operations memFS_dir_operations = {
         .open           = dcache_dir_open,
         .release        = dcache_dir_close,
         .llseek         = dcache_dir_lseek,
         .read           = memfs_read_dir,
         .readdir        = memfs_readdir,
         //.readdir        = dcache_readdir,
         .fsync          = simple_sync_file,
};
static int memFS_fill_super(struct super_block *sb, void *data, int silent)
{
	struct inode *root_inode;
	struct dentry *root_dentry;

	////////printk(LOG_LEVEL "----------------------FILL SUPER ----------------\n");
	sb->s_maxbytes = MAX_LFS_FILESIZE;
	sb->s_blocksize = PAGE_CACHE_SIZE;
	sb->s_blocksize_bits = PAGE_CACHE_SHIFT;
	sb->s_magic = MEMFS_MAGIC;
	sb->s_op = &memFS_ops;

	/* mode = directory & access rights (755) */
	root_inode = memFS_get_inode(sb,
			S_IFDIR | S_IRWXU | S_IRGRP |
			S_IXGRP | S_IROTH | S_IXOTH);
	if (!root_inode)
		goto out_no_root;

	root_dentry = d_alloc_root(root_inode);
	//root_dentry = d_make_root(root_inode);
	if (!root_dentry)
		goto out_no_root;
	sb->s_root = root_dentry;
	
	return 0;

out_no_root:
	iput(root_inode);
	return -ENOMEM;
}


int memFS_get_sb(struct file_system_type *fs_type,
		int flags, const char *dev_name, void *data, struct vfsmount *mnt)
{
	return get_sb_nodev(fs_type, flags, data, memFS_fill_super, mnt);
}

/*
static struct dentry *memFS_mount(struct file_system_type *fs_type,
		int flags, const char *dev_name, void *data)
{

	//return get_sb_nodev(fs_type, flags, data, memFS_fill_super, NULL);
	return mount_nodev(fs_type, flags, data, memFS_fill_super);
}

*/
static struct file_system_type memFS_fs_type = {
	.owner	= THIS_MODULE,
	.name 	= "memfs",
	.get_sb = memFS_get_sb,
	//.mount = memFS_mount,
	.kill_sb = kill_litter_super,
};



static int __init memFS_init(void)
{
	int err;
	int i, j;

	err = register_filesystem(&memFS_fs_type);
	if (err) {
		////////printk(LOG_LEVEL "register_filesystem failed\n");
		return err;
	}

	redis = kmalloc(NR_SERVERS * sizeof(struct redis_servers), GFP_KERNEL);
	mr_sem = kmalloc(NR_SERVERS * sizeof(struct semaphore), GFP_KERNEL);

	/**
	 * init the redis distributed communication data structure
	 * sockets as well
	 */
	for (i = 0; i < NR_SERVERS; i++){
		
		//initilize the number of concurent access to a server
		sema_init(&mr_sem[i], NR_CLIENTS); 
		sprintf(redis[i].ip, "%s", REDIS_SERVER_IP[i]);
		for (j = 0; j < NR_CLIENTS; j++){
			redis_init(i, j);
			redis[i].lock[j] = 0; //free
			redis[i].fd[j] = -1;
			redis[i].inode_no[j] = -1;
			redis[i].taint[j] = -1;
			redis[i].pid[j] = -1;

			redis[i].f_size[j] = 0;
			memset(redis[i].f_name[j], 0, 500 * sizeof(char));
			
			redis[i].buffer[j] =(char *) kmalloc(CHUNK * sizeof(char), GFP_KERNEL);
			memset(redis[i].buffer[j], 0, CHUNK * sizeof(char));
			redis[i].buf_pos[j] = 0;
		}
		////////printk(LOG_LEVEL "-----NR_SERVERS: %d---------------NR_CLIENTS:%d--------TOTAL:%d-\n", NR_SERVERS, NR_CLIENTS, NR_SERVERS*NR_CLIENTS);
	}
	
	return 0;
}

static void __exit memFS_exit(void)
{
	int i,j;

	//close communication with redis servers
	for (i = 0; i < NR_SERVERS; i++){
		for(j = 0; j < NR_CLIENTS; j++){
			redis_exit(i,j);
			kfree(redis[i].buffer[j]);
		}
	}

	kfree(redis);
	kfree(mr_sem);

	unregister_filesystem(&memFS_fs_type);

}

module_init(memFS_init);
module_exit(memFS_exit);
